﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DBHelper
{
    public sealed class DBHelper
    {

        #region Variable Declaration

        public string vConnectioString = string.Empty;
        //private DbProviderFactory vFactory = null;
        public SqlConnection vSqlCon = null;
        public SqlCommand vSqlCmd = null;
        public CommandType vCmdType;
        public string vQuery = string.Empty;
        public int vCmdTimeOut = 0;

        #endregion


        #region DBHelper

        public DBHelper(string _ConnectionString)
        {
            if (_ConnectionString.Trim().Length <= 0)
                throw new Exception("Connection String Not Supplied");
            this.vConnectioString = _ConnectionString;
            try
            {
                vSqlCon = mCreateConnection();
            }
            catch (Exception vEx)
            {
                throw vEx;
            }
        }

        #endregion


        #region CreateConnection

        private SqlConnection mCreateConnection()
        {
            vSqlCon = new SqlConnection();
            try
            {
                vSqlCon.ConnectionString = vConnectioString;
            }
            catch (Exception vEx)
            {
                throw vEx;
            }
            return vSqlCon;
        }

        #endregion


        #region CreateCommand

        public void mCreateCommand(CommandType _CmdType, string _Query)
        {
            if (_Query.Trim().Length <= 0)
                throw new Exception("Query is not Supplied");
            this.vCmdType = _CmdType;
            this.vQuery = _Query;
            try
            {
                vSqlCmd = new SqlCommand();
                vSqlCmd.CommandType = _CmdType;
                vSqlCmd.CommandText = _Query;
            }
            catch (Exception vEx)
            {
                throw vEx;
            }
        }

        public void mCreateCommand(CommandType _CmdType, string _Query, int _CmdTimeOut)
        {
            mCreateCommand(_CmdType, _Query);
            if (_CmdTimeOut > 0)
                vSqlCmd.CommandTimeout = _CmdTimeOut;
        }

        #endregion


        #region CreateParameter

        public SqlParameter mCreateParameter(string _ParameterName, SqlDbType _DBType, ParameterDirection _ParameterDirection, object _ParameterValue)
        {
            SqlParameter vParameter = new SqlParameter();
            vParameter.SqlDbType = _DBType;
            vParameter.ParameterName = _ParameterName;
            vParameter.Direction = _ParameterDirection;
            vParameter.Value = _ParameterValue;
            return vParameter;
        }

        public SqlParameter mCreateParameter(string _ParameterName, SqlDbType _DBType, int _Size, ParameterDirection _ParameterDirection, object _ParameterValue)
        {
            SqlParameter vParameter = new SqlParameter();
            vParameter.SqlDbType = _DBType;
            vParameter.Size = _Size;
            vParameter.ParameterName = _ParameterName;
            vParameter.Direction = _ParameterDirection;
            vParameter.Value = _ParameterValue;
            return vParameter;
        }

        #endregion


        #region AddParameter

        public void mAddParameter(string _ParameterName, SqlDbType _DBType, ParameterDirection _ParameterDirection, object _ParameterValue)
        {
            if (vSqlCmd == null)
                mCreateCommand(this.vCmdType, this.vQuery, this.vCmdTimeOut);
            vSqlCmd.Parameters.Add(mCreateParameter(_ParameterName, _DBType, _ParameterDirection, _ParameterValue));
        }

        public void mAddParameter(string _ParameterName, SqlDbType _DBType, int _Size, ParameterDirection _ParameterDirection, object _ParameterValue)
        {
            if (vSqlCmd == null)
                mCreateCommand(this.vCmdType, this.vQuery, this.vCmdTimeOut);
            vSqlCmd.Parameters.Add(mCreateParameter(_ParameterName, _DBType, _Size, _ParameterDirection, _ParameterValue));
        }


        #endregion


        #region Execute

        public DataSet Execute()
        {
            try
            {
                if (vSqlCon.State == ConnectionState.Open)
                    vSqlCon.Close();
                //vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                SqlDataAdapter vAdapter = new SqlDataAdapter();
                vAdapter.SelectCommand = vSqlCmd;
                DataSet vDataSet = new DataSet();
                vAdapter.Fill(vDataSet);
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vDataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                }
            }
        }

        public DataTable ExecuteDT()
        {
            try
            {
                if (vSqlCon.State == ConnectionState.Open)
                    vSqlCon.Close();
                //vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                SqlDataAdapter vAdapter = new SqlDataAdapter();

                vAdapter.SelectCommand = vSqlCmd;
                DataTable vDataTable = new DataTable();
                vAdapter.Fill(vDataTable);
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vDataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        public DataTable ExecuteDT(out string _MSG, string _MessageParameter)
        {
            try
            {
                if (vSqlCon.State == ConnectionState.Open)
                    vSqlCon.Close();
                //vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                SqlDataAdapter vAdapter = new SqlDataAdapter();

                vAdapter.SelectCommand = vSqlCmd;
                DataTable vDataTable = new DataTable();
                vAdapter.Fill(vDataTable);
                _MSG = vSqlCmd.Parameters[_MessageParameter].Value.ToString();
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vDataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        public DataTable ExecuteDTandOutparam(out long _ID, string _OutParameter, out string _MSG, string _MessageParameter)
        {
            try
            {
                if (vSqlCon.State == ConnectionState.Open)
                    vSqlCon.Close();
                //vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                SqlDataAdapter vAdapter = new SqlDataAdapter();

                vAdapter.SelectCommand = vSqlCmd;
                DataTable vDataTable = new DataTable();
                vAdapter.Fill(vDataTable);
                _ID = Int64.Parse(vSqlCmd.Parameters[_OutParameter].Value.ToString());
                _MSG = vSqlCmd.Parameters[_MessageParameter].Value.ToString();
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vDataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        #endregion


        #region ExecuteScalar

        /// <summary>
        /// Method ExecuteScalar
        /// to execute and receive a single value in object
        /// </summary>
        /// <returns>
        /// object type
        /// </returns>
        public object ExecuteScalar()
        {
            try
            {
                object vResult;
                if (vSqlCon.State == ConnectionState.Open)
                    vSqlCon.Close();
                vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                vResult = vSqlCmd.ExecuteScalar();
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        #endregion


        #region ExecuteNonQuery   

        /// <summary>
        ///  ExecuteNonQuery 
        /// to execute insert, update, delete statements
        /// </summary>
        /// <returns>
        /// int type
        /// </returns>
        public int ExecuteNonQuery()
        {
            try
            {
                int vResult;
                if (vSqlCon.State == ConnectionState.Open)
                    vSqlCon.Close();
                vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                vResult = vSqlCmd.ExecuteNonQuery();
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Executes a non query like insert and update statement
        /// </summary>
        /// <param name="id"></param>
        /// <param name="outParameter"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(out int _ID, string _OutParameter)
        {
            try
            {
                int vResult;
                if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                vResult = vSqlCmd.ExecuteNonQuery();
                _ID = int.Parse(vSqlCmd.Parameters[_OutParameter].Value.ToString());
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Executes a non query like insert and update statement
        /// </summary>
        /// <param name="id"></param>
        /// <param name="outParameter"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(out int _ID, string _OutParameter, out string _MSG, string _MessageParameter)
        {
            try
            {
                int vResult;
                if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                vResult = vSqlCmd.ExecuteNonQuery();
                _ID = int.Parse(vSqlCmd.Parameters[_OutParameter].Value.ToString());
                _MSG = vSqlCmd.Parameters[_MessageParameter].Value.ToString();
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Executes a non query like insert and update statement
        /// </summary>
        /// <param name="id"></param> as Out Long
        /// <param name="outParameter"></param>
        /// <returns></returns>
        public int ExecuteNonQuery_Long(out long _ID, string _OutParameter, out string _MSG, string _MessageParameter)
        {
            try
            {
                int vResult;
                if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                vResult = vSqlCmd.ExecuteNonQuery();
                _ID = Int64.Parse(vSqlCmd.Parameters[_OutParameter].Value.ToString());
                _MSG = vSqlCmd.Parameters[_MessageParameter].Value.ToString();
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Executes a non query like insert and update statement
        /// </summary>
        /// <param name="id"></param>
        /// <param name="outParameter"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(out string _MSG, string _MessageParameter)
        {
            try
            {
                int vResult;
                if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                vSqlCon.Open();
                vSqlCmd.Connection = vSqlCon;
                // var returnParameter = vSqlCmd.Parameters.Add("returnParameter ", SqlDbType.Int);
                //  returnParameter.Direction = ParameterDirection.ReturnValue;
                vResult = vSqlCmd.ExecuteNonQuery();
                _MSG = vSqlCmd.Parameters[_MessageParameter].Value.ToString();
                //  vResult = (int)(vSqlCmd.Parameters["returnParameter"].Value);
                //if (vSqlCon.State == ConnectionState.Open) vSqlCon.Close();
                return vResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (vSqlCon != null)
                {
                    if (vSqlCon.State == ConnectionState.Open)
                        vSqlCon.Close();

                    vSqlCon.Dispose();
                    vSqlCmd.Dispose();
                }
            }
        }

        #endregion

    }
}
