﻿


// Progress bar---------------Start Here--------------------------------------------------------------------->
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
var progressEnd = 60; // set to number of progress <span>'s.
var progressColor = 'green'; // set to progress bar color
var progressInterval = 100; // set to time between updates (milli-seconds)

var progressAt = progressEnd;
var progressTimer;

function progress_clear() {
    for (var i = 1; i <= progressEnd; i++) document.getElementById('progress' + i).style.backgroundColor = 'transparent';
    progressAt = 0;
}

function progress_stop() {
    clearTimeout(progressTimer);
    progress_clear();
    document.getElementById('showbar').style.visibility = 'hidden';
}

function progress_update() {
    document.getElementById('showbar').style.visibility = 'visible';
    progressAt++;
    if (progressAt > progressEnd)
        progress_clear();
    else
        document.getElementById('progress' + progressAt).style.backgroundColor = progressColor;
    progressTimer = setTimeout('progress_update()', progressInterval);
}

function ProgressBarDesign() {
    var id = 60;
    for (var i = 1; i < id; i++) {
        var progressDesign1 = "<span id='progress" + i + "'>&nbsp; &nbsp;</span>";
        var j = i + 1;
        var progressDesign2 = "<span id='progress" + j + "'>&nbsp; &nbsp;</span>";
        $("#showbar").append(progressDesign1 + progressDesign2);

    };

}


// Progress bar---------------END Here--------------------------------------------------------------------->

function LayoutShow() {
    $('#navbar').show();
    $('#sidebar').show();
    $('#LayoutFooter').show();
    $('#sidebar-collapse').show();
}

function LayoutHide() {
    $('#navbar').hide();
    $('#sidebar').hide();
    $('#LayoutFooter').hide();
    $('#sidebar-collapse').hide();
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function BindDropdown(Data, control) {

    $("#" + control + " :gt(0)").remove();
    if (Data != null) {
        $.each(Data, function (i) {
            var optionhtml = '<option value="' + Data[i].ID + '">' + Data[i].Name + '</option>';
            $("#" + control).append(optionhtml);
        });
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetDropdownValues(ctrl) {
    var values = new Array();
    $('#' + ctrl + ' option').each(function () {
        values.push(this.value);
    });
    return values;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function ajaxGETResonse(url, sucessFn, failureFn) {
    $.ajax(
    {
        type: 'GET',
        dataType: 'json',
        url: url,
        success: sucessFn,
        error: failureFn
    });
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function ajaxGETResonse(url, sucessFn, failureFn, param) {
    $.ajax(
    {
        type: 'GET',
        dataType: 'json',
        url: url,
        data: param,
        success: sucessFn,
        error: failureFn
    });
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function errorfun() {
    DialogWarningMessage('Error in fetching data..Please Contact System Admin');
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function consolidateElements(divId) {
    var obj = {};
    //Find all input elements
    var panel = $("#" + divId);
    var inpts = panel.find("input,textarea,select");
    // var txtArea = panel.find("textarea");

    if (inpts.length > 0) {
        $.each(inpts, function (key, value) {
            if (value.type == 'text' ||
                value.type == 'file' ||
                value.type == 'hidden' ||
                value.type == 'password' ||
                value.type == 'textarea' ||
                value.type == 'select' ||
                value.type == 'date' ||
                 value.type == 'select-one') {
                if (value.value != '')
                    obj[value.id] = value.value;
            }
            else if (value.type == 'checkbox' || value.type == 'radio') {
                if ($("#" + value.id).is(":checked")) {
                    obj[value.name] = true;
                }
                else
                    obj[value.name] = false;
            }
        });
    }
    //    if (txtArea.length > 0) {
    //        $.each(txtArea, function (key, value) {
    //            if (value.type == 'textarea' ) {
    //                obj[value.id] = value.value;
    //            }
    //        });
    //    }
    //    //find all select elements
    //    var selects = panel.find("select");
    //    if (selects.length > 0) {
    //        $.each(selects, function (key, value) {
    //            obj[value.id] = value.value;
    //        });
    //    }
    return obj;


}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function consolidateRequiredElements(divId) {
    var obj = [];
    //Find all input elements
    var panel = $("#" + divId);
    var inpts = panel.find("input,textarea,select").filter('[required]:visible');

    // alert(inpts);
    // var txtArea = panel.find("textarea");

    if (inpts.length > 0) {
        $.each(inpts, function (key, value) {
            if (value.type == 'text' ||
                value.type == 'file' ||
                value.type == 'hidden' ||
                value.type == 'password' ||
                value.type == 'textarea' ||
                value.type == 'select' ||
                value.type == 'date' ||
                 value.type == 'select-one') {
                if (value.id != '')
                    obj.push(value.id);
                //  obj[value.id] = value.value;

            }
            //else if (value.type == 'checkbox' || value.type == 'radio') {
            //    if ($("#" + value.id).is(":checked")) {
            //        obj[value.name] = value.value;
            //    }
            //}
        });
    }

    return obj;


}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function requiredFiledsValidate(divId, errorID) {
    if (errorID == null || errorID == '' || errorID == undefined)
        errorID = 'divBPWanringBox';
    var _controlFields = consolidateRequiredElements(divId);
    var _valid = true;
    var _msg = '<br>';
    if (_controlFields.length > 0) {
        $.each(_controlFields, function (i, item) {
            var _value = $('#' + item).val();
            var _name = $('#' + item).attr("name");
            if (_value == "" || _value == null) {
                //alert('Please Provide ' + _name + ' Value');
                _msg = _msg + 'Please Provide ' + _name + ' Value. <br> ';
                _valid = false;
            }

        });
        if (_msg != '<br>')
            $('#' + errorID).html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
    }
    return _valid;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function CustomFiledsValidate(_controlFields, errorID) {
    if (errorID == null || errorID == '' || errorID == undefined)
        errorID = 'divWanringBox';
    // var _controlFields = consolidateRequiredElements(divId);
    var _valid = true;
    var _msg = '<br>';
    if (_controlFields.length > 0) {
        $.each(_controlFields, function (i, item) {
            var _value = $('#' + item[0]).val();
            //var _name = $('#' + item).attr("name");
            if (_value == "" || _value == null) {
                //alert('Please Provide ' + _name + ' Value');
                _msg = _msg + item[1] + ' <br> ';
                _valid = false;
            }

        });
        if (_msg != '<br>')
            $('#' + errorID).html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
    }
    return _valid;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function CustomFiledsValidateDialog(_controlFields) {

    var _valid = true;
    var _msg = '<br>';
    if (_controlFields.length > 0) {
        $.each(_controlFields, function (i, item) {
            var _value = $('#' + item[0]).val();
            //var _name = $('#' + item).attr("name");
            if (_value == "" || _value == null) {
                //alert('Please Provide ' + _name + ' Value');
                _msg = _msg + item[1] + ' <br> ';
                _valid = false;
            }

        });
        if (_msg != '<br>') {
            // $('#' + errorID).html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
            //$('#divAlertMessageBoxContent').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
            $('#divAlertMessageBoxContent').html('<div class="alert alert-warning"><strong style="align:center">Warning!</strong> ' + _msg + '</div>');

            $('#divAlertMessageBox').modal('show')
        }

    }
    return _valid;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function CustomFiledsResultDialog(_controlFields) {

    var _valid = true;
    var _msg = '<br>';
    if (_controlFields.length > 0) {
        $.each(_controlFields, function (i, item) {
            var _value = $('#' + item[0]).val();
            //var _name = $('#' + item).attr("name");
            if (_value == "" || _value == null) {
                //alert('Please Provide ' + _name + ' Value');
                _msg = _msg + item[1] + ' <br> ';
                _valid = false;
            }

        });
        if (_msg != '<br>') {
            // $('#' + errorID).html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
            //$('#divAlertMessageBoxContent').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
            $('#divAlertMessageBoxContent').html('<div class="alert alert-success"><strong style="align:center"></strong> ' + _msg + '</div>');

            $('#divAlertMessageBox').modal('show')
        }

    }
    return _valid;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function MultipleMsgsValidateDialog(_controlFields) {

    var _valid = true;
    var _msg = '<br>';
    if (_controlFields.length > 0) {
        $.each(_controlFields, function (i, item) {
            _msg = _msg + item + ' <br> ';
        });
        if (_msg != '<br>') {
            // $('#' + errorID).html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
            //$('#divAlertMessageBoxContent').html('<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + _msg + '</div>');
            $('#divAlertMessageBoxContent').html('<div class="alert alert-warning"><strong style="align:center">Warning!</strong> ' + _msg + '</div>');

            $('#divAlertMessageBox').modal('show')
        }

    }
    return _valid;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function ShowResultMessage(controlId, msg) {
    //  $('#' + controlId).html('<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><img src="/Content/images/cross.png" alt="Cross"></button><img src="/Content/images/check.png" alt="Check">' + msg + '</div>').attr("tabindex", -1).focus();;
    $('#' + controlId).html('<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><img src="/Content/images/cross.png" alt="Cross"></button><img src="/Content/images/check.png" alt="Check">' + msg + '</div>').attr("tabindex", -1).focus();;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function DialogResultMessage(msg) {
    //var msghtml = '<div class="alert alert-block alert-success"></button><img src="/Content/images/check.png" alt="Check">' + msg + '</div>';
    var msghtml = '<div class="alert alert-block alert-success">' + msg + '</div>';
    $('#divAlertMessageBoxContent').html(msghtml);

    $('#divAlertMessageBox').modal('show')                // initializes and invokes show immediately

}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function DialogErrorMessage(msg) {
    var msghtml = '<div class="alert alert-block alert-danger"></button><img src="/Content/images/check.png" alt="Check">' + msg + '</div>';

    $('#divAlertMessageBoxContent').html(msghtml);

    $('#divAlertMessageBox').modal('show')                // initializes and invokes show immediately

}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function DialogWarningMessage(msg) {
    //var msghtml = '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button><strong style="align:center">Warning!</strong> ' + msg + '</div>';
    var msghtml = '<div class="alert alert-warning"><strong style="align:center">Warning!</strong> ' + msg + '</div>';

    $('#divAlertMessageBoxContent').html(msghtml);

    $('#divAlertMessageBox').modal('show')                // initializes and invokes show immediately

}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function ShowErrorMessage(controlId, msg) {
    $('#' + controlId).html('<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert"><img src="/Content/images/cross.png" alt="Cross"></button><img src="/Content/images/check.png" alt="Check">' + msg + '</div>').attr("tabindex", -1).focus();;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function ShowWanringMessage(controlId, msg) {
    $('#' + controlId).html('<div class="alert alert-block alert-warning"><button type="button" class="close" data-dismiss="alert"><img src="/Content/images/cross.png" alt="Cross"></button><img src="/Content/images/check.png" alt="Check">' + msg + '</div>').attr("tabindex", -1).focus();;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function JSONDate(dateStr, control) {
    // alert(dateStr);
    if (dateStr != null) {
        dateStr = new Date(dateStr);
        var month = dateStr.getMonth();
        var day = dateStr.getDate();
        month = month + 1;
        month = month + "";
        if (month.length == 1) {
            month = "0" + month;
        }
        day = day + "";
        if (day.length == 1) {
            day = "0" + day;
        }
        var date = (dateStr.getFullYear() + '-' + month + '-' + day);
        $("#" + control).val(date);
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetJSONDate(dateStr) {
    // alert(dateStr);
    if (dateStr != null) {
        dateStr = new Date(dateStr);
        var month = dateStr.getMonth();
        var day = dateStr.getDate();
        month = month + 1;
        month = month + "";
        if (month.length == 1) {
            month = "0" + month;
        }
        day = day + "";
        if (day.length == 1) {
            day = "0" + day;
        }
        var date = (dateStr.getFullYear() + '-' + month + '-' + day);
        return date;
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Convert json date format to dd/mm/yy format
function JSONDate1(dateStr) {
    if (dateStr != "" && dateStr != null) {
        var m, day, hour, min, sec;
        jsonDate = dateStr;
        var d = new Date(parseInt(jsonDate.substr(6)));
        m = d.getMonth() + 1;
        if (m < 10)
            m = '0' + m
        if (d.getDate() < 10)
            day = '0' + d.getDate()
        else
            day = d.getDate();


        var date = (d.getFullYear() + '-' + m + '-' + day);
        return date;
    }
}

function JSONDate1WithCntrl(dateStr, control) {
    if (dateStr != "" && dateStr != null) {
        var m, day, hour, min, sec;
        jsonDate = dateStr;
        var d = new Date(parseInt(jsonDate.substr(6)));
        m = d.getMonth() + 1;
        m = GetMonthName(m);
        if (d.getDate() < 10)
            day = '0' + d.getDate()
        else
            day = d.getDate();


        var date = (day + '-' + m + '-' + d.getFullYear());
        //var date = (d.getFullYear() + '-' + m + '-' + day);
        $("#" + control).val(date);
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 3rdOct2015
---------------------------------------------------*/
//Convert json date format to 03-Oct-2015 format
function JSONDate2(dateStr) {
    if (dateStr != "" && dateStr != null) {
        var m, day, hour, min, sec;
        jsonDate = dateStr;
        var d = new Date(jsonDate.substr(0, 10));
        m = d.getMonth() + 1;
        m = GetMonthName(m);
        if (d.getDate() < 10)
            day = '0' + d.getDate()
        else
            day = d.getDate();


        var date = (day + '-' + m + '-' + d.getFullYear());
        //var date = (d.getFullYear() + '-' + m + '-' + day);
        return date;
    } else {
        return ''
    }
}

//Convert json date format to 03-Oct-2015 format
function JSONDate2WithCntrl(dateStr, control) {
    if (dateStr != "" && dateStr != null) {
        var m, day, hour, min, sec;
        jsonDate = dateStr;
        var d = new Date(JSONDate3(dateStr));
        // var d = new Date(jsonDate.substr(0, 10));
        m = d.getMonth() + 1;
        m = GetMonthName(m);
        if (d.getDate() < 10)
            day = '0' + d.getDate()
        else
            day = d.getDate();


        var date = (day + '-' + m + '-' + d.getFullYear());
        //var date = (d.getFullYear() + '-' + m + '-' + day);
        $("#" + control).val(date);
    } else {
        return ''
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Convert json date format to YYYY-MM-DD HH MM SS format
function JSONDateTime(dateStr) {
    if (dateStr != "" && dateStr != null) {
        var m, day, hour, min, sec;
        jsonDate = dateStr;
        // var d = new Date(parseInt(jsonDate.substr(6)));
        //var d = new Date(jsonDate.substr(0, 10) + " " + jsonDate.substr(11, 19)); //2015-09-18T15:42:14
        var d = new Date(jsonDate.substr(5, 2) + "/" + jsonDate.substr(8, 2) + "/" + jsonDate.substr(0, 4) + " " + jsonDate.substr(11, 8)); //2015-09-18T15:42:14
        //var t = new Date(jsonDate.substr(12, 19));
        // var dt = new Date(d + " " + t);
        // alert(d);
        m = d.getMonth() + 1;
        if (m < 10)
            m = '0' + m
        if (d.getDate() < 10)
            day = '0' + d.getDate()
        else
            day = d.getDate();

        hour = d.getHours();
        min = d.getMinutes();
        sec = d.getSeconds();

        if (min < 10)
            min = '0' + min

        if (sec < 10)
            sec = '0' + sec

        if (hour < 10)
            hour = '0' + hour
        var date = (day + '-' + GetMonthName(m) + '-' + d.getFullYear() + " " + hour + ':' + min + ':' + sec);
        //var date = (d.getFullYear() + '-' + m + '-' + day + " " + hour + ':' + min + ':' + sec);
        return date;
    }
    //else {
    //    return '';
    //}
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Convert json date format ("/Date(1425554876547)/") to YYYY-MM-DD HH MM SS format
function JSONDateTime1(dateStr) {
    if (dateStr != "" && dateStr != null) {
        var m, day, hour, min, sec;
        jsonDate = dateStr;
        var d = new Date(parseInt(jsonDate.substr(6)));
        //var d = new Date(jsonDate.substr(0, 10) + " " + jsonDate.substr(12, 19)); //2015-09-18T15:42:14
        //var t = new Date(jsonDate.substr(12, 19));
        // var dt = new Date(d + " " + t);
        // alert(d);
        m = d.getMonth() + 1;
        if (m < 10)
            m = '0' + m
        if (d.getDate() < 10)
            day = '0' + d.getDate()
        else
            day = d.getDate();

        hour = d.getHours();
        min = d.getMinutes();
        sec = d.getSeconds();

        if (min < 10)
            min = '0' + min

        if (sec < 10)
            sec = '0' + sec

        if (hour < 10)
            hour = '0' + hour
        var date = (day + '-' + GetMonthName(m) + '-' + d.getFullYear() + " " + hour + ':' + min + ':' + sec);
        //var date = (d.getFullYear() + '-' + m + '-' + day + " " + hour + ':' + min + ':' + sec);
        return date;
    }
}


// Bind date to span tag nagaraju
function JSONDateSpan(dateStr, control) {
    // alert(dateStr);
    if (dateStr != null) {
        dateStr = new Date(dateStr);
        var month = dateStr.getMonth();
        var day = dateStr.getDate();
        month = month + 1;
        month = month + "";
        if (month.length == 1) {
            month = "0" + month;
        }
        day = day + "";
        if (day.length == 1) {
            day = "0" + day;
        }
        var date = (dateStr.getFullYear() + '-' + month + '-' + day);
        $("#" + control).text(date);
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 30thMar2016
---------------------------------------------------*/
//Convert "30-Dec-2016" format to mm/dd/yyyy format
function JSONDate3(inStr) {
    if ((typeof inStr == 'undefined') || (inStr == null) ||
    (inStr.length <= 0)) {
        return '';
    }
    var year = inStr.substr(7, 4);
    var month = inStr.substr(3, 3);
    var day = inStr.substr(0, 2);
    // alert(Date.parse(GetMonthNumber(month) + '/' + day + '/' + year));
    return GetMonthNumber(month) + '/' + day + '/' + year;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Key Press Allow digits
function allowDigitsKeypressEvent(keyPressEvent) {
    var regex = new RegExp("^[0-9]+$");
    var str = String.fromCharCode(!keyPressEvent.charCode ? keyPressEvent.which : keyPressEvent.charCode);
    if (regex.test(str)) {
        return true;
    }
    keyPressEvent.preventDefault();
    return false;
    DialogWarningMessage('Please Enter Valid phone number');
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Key Press Allow digits
function allowDecimalKeypressEvent(keyPressEvent) {
    var regex = new RegExp("^[0-9.]+([.][0-9]+)?$");
    //var regex = new RegExp("^d{0,2}(\.\d{1,2})?");
    //var regex = "[0-9]+(\.[0-9][0-9]?)?";
    //var regex = "((\d+)((\.\d{1,2})?))$";
    var str = String.fromCharCode(!keyPressEvent.charCode ? keyPressEvent.which : keyPressEvent.charCode);
    if (regex.test(str)) {
        return true;
    }
    // str.value = str.slice(0, 2).join('.');
    keyPressEvent.preventDefault();
    return false;
    // alert('Please Enter Valid phone number');
}
function ValidateLimit(obj, maxchar) {
    if (this.id) obj = this;
    var remaningChar = maxchar - obj.value.length;
    //var lb = document.getElementById('<%= Label1.ClientID %>');
    //lb.innerHTML = remaningChar;


    if (remaningChar <= 0) {
        obj.value = obj.value.substring(maxchar, 0);
        lb.innerHTML = "0";
    }
}


/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function NotAllowDuplicateDotsKeyPress(key) {
    //  alert(key);
    var str = this.value;
    var a = str.split('.');
    if (a.length > 2) {
        this.value = a.slice(0, 2).join('.');//remove all after second dot
        //a[0] +='.'; this.value = a.join('');//only removes redundant dots
    }

}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Validation for AlphaNumeric 
var validatAlphaNumeric = function () {
    cString = customArea.val();
    console.log(cString)
    var patt = /[^0-9a-zA-Z]/
    if (!cString.match(patt)) {
        DialogWarningMessage("valid");
    } else {
        DialogWarningMessage("invalid");
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function onlyChardigits(e) {

    var keyVal = 0;
    if (!e) var e = window.event;
    if (!e.which) keyVal = e.keyCode;
    else keyVal = e.which;
    if ((keyVal != 27)) {
        if ((keyVal >= 65) && (keyVal <= 90) || (keyVal >= 97) && (keyVal <= 122) || keyVal == 8 || keyVal == 9 || keyVal == 32 || keyVal == 38 || keyVal == 45 || (keyVal >= 46 && keyVal <= 57) ||
			(keyVal >= 35 && keyVal < 37) ||
			keyVal == 39 || keyVal == 8 ||
			keyVal == 9) {
            return true;
        } else {
            keyVal = 0;
            return false;
        }
    }
    return false;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function onlyChars(e) {

    var keyVal = 0;
    if (!e) var e = window.event;
    if (!e.which) keyVal = e.keyCode;
    else keyVal = e.which;
    if ((keyVal != 27)) {
        if ((keyVal >= 65) && (keyVal <= 90) || (keyVal >= 97) && (keyVal <= 122) || keyVal == 8 || keyVal == 9 || keyVal == 32 || keyVal == 38 || keyVal == 45 || (keyVal >= 46 && keyVal <= 57)) {
            return true;
        } else {
            keyVal = 0;
            return false;
        }
    }
    return false;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function onlydigits(e) {
    var keyVal = 0;
    if (!e) var e = window.event;
    if (!e.which) keyVal = e.keyCode;
    else keyVal = e.which;
    if ((keyVal != 27)) {
        if ((keyVal >= 46 && keyVal <= 57) ||
			(keyVal >= 35 && keyVal < 37) ||
			keyVal == 39 || keyVal == 8 ||
			keyVal == 9) {
            return true;
        } else {
            keyVal = 0;
            return false;
        }
    }
    return false;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function IsAlphaNumeric(e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    //e.preventDefault();
    return false;
}

function checkUrl(url) {
    if (url != "") {
        var urlregex = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/
        if (!urlregex.test(url)) {
            DialogWarningMessage("Please enter valid Web Address");
            return false;
        } else {
            return true;
        }
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
Description : Only allow alphabates and space
---------------------------------------------------*/

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 32 || charCode == 38 || charCode == 45 || charCode == 46))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function RemoveFile(id) {
    if (id == '' || id == null || id == 'undefined') {
        DialogWarningMessage('Please Enter or Select Entity Type');
    } else {

        $.ajax({
            url: 'FileUpload/RemoveDocument',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data:
                { ID: id },

            //dataType: 'json',
            success: function (data) {

                // alert(data);


            },
            error: function (err) {
                // alert(err);
                alert("Hello");
                //err[0]
            }
        });

        // ajaxGETResonse("/FileUpload/GetDocuments", function (res) { alert(res); }, errorfun, { EntityType: type, EntityId: id });

    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function MakeDisableControls(divId) {

    //$("INPUT[type='text'],select").each(function () {
    //    $(this).attr("disabled", "disabled");
    //});


    $("INPUT[type='button']").each(function () {
        $(this).attr("disabled", "disabled");
    });

    $("INPUT[type='submit']").each(function () {
        $(this).attr("disabled", "disabled");
    });

    $("BUTTON[type='button']").each(function () {
        $(this).attr("disabled", "disabled");
        //$(this).hide();
    });

    //$('#txtManager').removeAttr('disabled'); // enable


    var obj = {};
    //Find all input elements
    var panel = $("#" + divId);
    var inpts = panel.find("input,textarea,select");
    // var txtArea = panel.find("textarea");

    if (inpts.length > 0) {
        $.each(inpts, function (key, value) {
            if (value.type == 'text' ||
                value.type == 'file' ||
                value.type == 'hidden' ||
                value.type == 'password' ||
                value.type == 'textarea' ||
                value.type == 'select' ||
                value.type == 'date' ||
                 value.type == 'select-one') {
                //if (value.value != '')
                //    obj[value.id] = value.value;

                $(this).attr("disabled", "disabled");
            }
            else if (value.type == 'checkbox' || value.type == 'radio') {
                //if ($("#" + value.id).is(":checked")) {
                //    obj[value.name] = value.value;
                //}
                $(this).attr("disabled", "disabled");
            }
        });

    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function Empty(id) {
    $('#' + id).html('');
}




/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//function MakeDisableControls(divId) {
//    $("#" + divId + " INPUT[type='button']").each(function () {
//        $(this).attr("disabled", "disabled");
//    });

//    var obj = {};
//    //Find all input elements
//    var panel = $("#" + divId);
//    var inpts = panel.find("input,textarea,select");
//    // var txtArea = panel.find("textarea");

//    if (inpts.length > 0) {
//        $.each(inpts, function (key, value) {
//            if (value.type == 'text' ||
//                value.type == 'file' ||
//                value.type == 'hidden' ||
//                value.type == 'password' ||
//                value.type == 'textarea' ||
//                value.type == 'select' ||
//                value.type == 'date' ||
//                 value.type == 'select-one') {
//                //if (value.value != '')
//                //    obj[value.id] = value.value;

//                $(this).attr("disabled", "disabled");
//            }
//            else if (value.type == 'checkbox' || value.type == 'radio') {
//                //if ($("#" + value.id).is(":checked")) {
//                //    obj[value.name] = value.value;
//                //}
//                $(this).attr("disabled", "disabled");
//            }
//        });

//    }
//}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function MakeEnableControls(divId) {
    $("#" + divId + " INPUT[type='button']").each(function () {
        $(this).removeAttr("disabled");
    });

    var obj = {};
    //Find all input elements
    var panel = $("#" + divId);
    var inpts = panel.find("input,textarea,select");
    // var txtArea = panel.find("textarea");

    if (inpts.length > 0) {
        $.each(inpts, function (key, value) {
            if (value.type == 'text' ||
                value.type == 'file' ||
                value.type == 'hidden' ||
                value.type == 'password' ||
                value.type == 'textarea' ||
                value.type == 'select' ||
                value.type == 'date' ||
                 value.type == 'select-one') {
                //if (value.value != '')
                //    obj[value.id] = value.value;

                $(this).removeAttr("disabled");
            }
            else if (value.type == 'checkbox' || value.type == 'radio') {
                //if ($("#" + value.id).is(":checked")) {
                //    obj[value.name] = value.value;
                //}
                $(this).removeAttr("disabled");
            }
        });

    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function ClearControls(divId) {
    $("#" + divId + " INPUT[type='button']").each(function () {
        //  if (this.id == corpMaindiv) { alert(); }
        $(this).removeAttr("disabled");
    });

    var obj = {};
    //Find all input elements
    var panel = $("#" + divId);
    var inpts = panel.find("input,textarea,select");
    // var txtArea = panel.find("textarea");

    if (inpts.length > 0) {
        $.each(inpts, function (key, value) {
            if (value.type == 'text' ||
                value.type == 'file' ||
                value.type == 'hidden' ||
                value.type == 'password' ||
                value.type == 'textarea' ||
                value.type == 'select' ||
                value.type == 'date' ||
                // value.type == 'select-multiple'||
                 value.type == 'select-one') {
                //if (value.value != '')
                //    obj[value.id] = value.value;

                $(this).val('');
            }
            else if (value.type == 'checkbox' || value.type == 'radio') {
                //if ($("#" + value.id).is(":checked")) {
                //    obj[value.name] = value.value;
                //}
                $(this).attr('checked', false);
            }
            else if (value.type == 'select-multiple') {
                // $(this)[0].sumo.unload();

                var obj = [];
                $('#' + this.id + ' option:selected').each(function () {
                    obj.push($(this).index());
                });

                for (var i = 0; i < obj.length; i++) {
                    //$(this).sumo.unSelectItem(obj[i]);
                    $('#' + this.id)[0].sumo.unSelectItem(obj[i])
                }
            }
        });

    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function LoadSumoselectCheckbox(options, _controlId) {
    if (options != null && options != undefined) {
        var obj = [];
        $('#' + _controlId + ' option:selected').each(function () {
            obj.push($(this).index());
        });

        for (var i = 0; i < obj.length; i++) {
            $('#' + _controlId)[0].sumo.unSelectItem(obj[i])
        }

        var _ids = [];
        if (options != '')
            _ids = options.split(',');
        $.each(_ids, function (a, b) {
            $('#' + _controlId)[0].sumo.selectItem($('#' + _controlId + '  option[value=' + b + ']').index());
        });
    }

}


/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetMonthNumber(MonthName) {
    var MonthId = null;
    switch (MonthName) {
        case "Jan":
            MonthId = "01";
            break;
        case "Feb":
            MonthId = "02";
            break;
        case "Mar":
            MonthId = "03";
            break;
        case "Apr":
            MonthId = "04";
            break;
        case "May":
            MonthId = "05";
            break;
        case "Jun":
            MonthId = "06";
            break;
        case "Jul":
            MonthId = "07";
            break;
        case "Aug":
            MonthId = "08";
            break;
        case "Sep":
            MonthId = "09";
            break;
        case "Oct":
            MonthId = "10";
            break;
        case "Nov":
            MonthId = "11";
            break;
        case "Dec":
            MonthId = "12";
            break;

    }
    return MonthId;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetMonthName(id) {
    var MonthName;
    switch (parseInt(id)) {
        case 1: MonthName = "Jan"; break;
        case 2: MonthName = "Feb"; break;
        case 3: MonthName = "Mar"; break;
        case 4: MonthName = "Apr"; break;
        case 5: MonthName = "May"; break;
        case 6: MonthName = "Jun"; break;
        case 7: MonthName = "Jul"; break;
        case 8: MonthName = "Aug"; break;
        case 9: MonthName = "Sep"; break;
        case 10: MonthName = "Oct"; break;
        case 11: MonthName = "Nov"; break;
        case 12: MonthName = "Dec"; break;
    }
    return MonthName;
}


/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Get Age From DOB(2015-07-30) 
---------------------------------------------------*/

function GetAgeandType(Dob) {
    // "1985-05-06"
    // "10-06-2014"
    var mdate = Dob;
    // alert(mdate);
    //    var dayThen = mdate.substr(0, 2);
    //    var monthThen = mdate.substr(3, 2);
    //    var yearThen = mdate.substr(6, 4);


    //if (mdate.length == 11) {
    //    var month = mdate.substr(5, 2);
    //    var dayNo = mdate.substr(8, 2);
    //    var monthNo = GetMonthNumber(month);
    //    var yearNo = mdate.substr(0, 4);

    //    // mdate = yearNo + "-" + dayNo + "-" + monthNo;
    //    mdate = yearNo + "-" + monthNo + "-" + dayNo;
    //}
    var dayThen = mdate.substr(8, 2);

    var monthThen = mdate.substr(5, 2);
    var yearThen = mdate.substr(0, 4);
    var today = new Date();
    var birthday = new Date(yearThen, monthThen - 1, dayThen);
    var differenceInMilisecond = today.valueOf() - birthday.valueOf();
    //    var year_age = Math.floor(differenceInMilisecond / 31536000000);
    //  var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);

    var year_age = Math.floor(differenceInMilisecond / (1000 * 60 * 60 * 24 * 365.25));
    var day_age = Math.floor((differenceInMilisecond % (1000 * 60 * 60 * 24 * 365.25)) / 86400000);

    var rAge = null;
    var rAgeType = null;
    //    if ((today.getMonth() == birthday.getMonth()) && (today.getDate() == birthday.getDate())) {
    //        alert("Happy B'day!!!");
    //    }
    var month_age = Math.floor(day_age / 30);
    day_age = day_age % 30;
    if (year_age > 0) {
        rAge = year_age;
        rAgeType = 'Year(s)';
    }
    else if (month_age > 0) {
        rAge = month_age;
        rAgeType = 'Month(s)';
    }
    else if (day_age >= 0) {
        rAge = day_age;
        rAgeType = 'Day(s)';
    }
    var Age = rAge + ',' + rAgeType;
    return Age;
}


/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Get Age From DOB(18-Nov-2015) 
---------------------------------------------------*/

function GetAgeandType1(Dob) {
    // "1985-05-06"
    // "10-06-2014"
    var mdate = Dob;
    // alert(mdate);
    //    var dayThen = mdate.substr(0, 2);
    //    var monthThen = mdate.substr(3, 2);
    //    var yearThen = mdate.substr(6, 4);


    //if (mdate.length == 11) {
    //    var month = mdate.substr(5, 2);
    //    var dayNo = mdate.substr(8, 2);
    //    var monthNo = GetMonthNumber(month);
    //    var yearNo = mdate.substr(0, 4);

    //    // mdate = yearNo + "-" + dayNo + "-" + monthNo;
    //    mdate = yearNo + "-" + monthNo + "-" + dayNo;
    //}
    var dayThen = mdate.substr(0, 2);

    var monthThen = GetMonthNumber(mdate.substr(3, 3));
    var yearThen = mdate.substr(7, 4);
    var today = new Date();
    var birthday = new Date(yearThen, monthThen - 1, dayThen);
    var differenceInMilisecond = today.valueOf() - birthday.valueOf();
    //    var year_age = Math.floor(differenceInMilisecond / 31536000000);
    //  var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);

    var year_age = Math.floor(differenceInMilisecond / (1000 * 60 * 60 * 24 * 365.25));
    var day_age = Math.floor((differenceInMilisecond % (1000 * 60 * 60 * 24 * 365.25)) / 86400000);

    var rAge = null;
    var rAgeType = null;
    //    if ((today.getMonth() == birthday.getMonth()) && (today.getDate() == birthday.getDate())) {
    //        alert("Happy B'day!!!");
    //    }
    var month_age = Math.floor(day_age / 30);
    day_age = day_age % 30;
    if (year_age > 0) {
        rAge = year_age;
        rAgeType = 'Year(s)';
    }
    else if (month_age > 0) {
        rAge = month_age;
        rAgeType = 'Month(s)';
    }
    else if (day_age >= 0) {
        rAge = day_age;
        rAgeType = 'Day(s)';
    }
    var Age = rAge + ',' + rAgeType;
    return Age;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Getting Name By Passing Id,Data  
---------------------------------------------------*/
function getNamepropwithId(id, data) {

    var name = '';
    if (data != null) {
        $.each(data, function (i, item) {
            if (item.ID == id) {
                name = item.Name;
            }
        });
    }
    return name;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Getting Multiple Names By Passing Ids,Data
---------------------------------------------------*/
function getNamelistpropwithIds(ids, data) {
    if (ids == null) {
        return '';
    } else {
        var idlist = [];
        idlist = ids.split(',');
        var name = '';
        if (data != null) {
            $.each(idlist, function (j, idprop) {
                $.each(data, function (i, item) {
                    if (item.ID == idprop) {
                        if (name != '')
                            name = name + ',' + item.Name;
                        else
                            name = item.Name;
                    }
                });
            })
        }
        //if (data != null) {
        //    $.each(data, function (i, item) {
        //        if (item.ID == id) {
        //            name = item.Name;
        //        }
        //    });
        //}
        return name;
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function CommomErrorFunction(msg, id) {
    ShowErrorMessage(id, msg);
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function DialogCommomErrorFunction(msg) {
    DialogErrorMessage(msg);
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Getting Dropdown Html  using Camma saparated value
---------------------------------------------------*/
function GetCSVDropdownHtml(ids) {
    var html = '';
    if (ids != null && ids != '') {
        var idlist = [];
        if (ids != '')
            idlist = ids.split(',');

        if (idlist != null) {
            $.each(idlist, function (i, item) {

                html = html + '<option value="' + item + '">' + item + '</option>';

            });
        }

    }
    return html;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 20th Nov 2015
Description: Binding Dropdown using Camma saparated value by passing Comma Separated string and Control ID
---------------------------------------------------*/
function BindDropdownCSV(ids, controlId) {
    var html = '';
    if (ids != null && ids != '') {
        var idlist = [];
        if (ids != '')
            idlist = ids.split(',');
        var html = '';
        if (idlist != null) {
            $.each(idlist, function (i, item) {

                html = html + '<option value="' + item + '">' + item + '</option>';

            });
        }
        $('#' + controlId).html(html);
    }
    // return html;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 24th Nov 2015
Description: Binding Dropdown using Camma saparated value by passing Comma Separated string and Control ID
---------------------------------------------------*/
function BindDropdownCSVwithOptionselect(ids, controlId) {
    var idlist = [];
    if (ids != '' && ids != null)
        idlist = ids.split(',');
    var html = '<option value="">Select</option>';
    if (idlist != null) {
        $.each(idlist, function (i, item) {

            html = html + '<option value="' + item + '">' + item + '</option>';

        });
    }
    $('#' + controlId).html(html);
    // return html;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Getting html By Passing Ids,Data
---------------------------------------------------*/
function FormatHtml_DropdownByIds(ids, data) {
    var idlist = [];
    if (ids != '')
        idlist = ids.split(',');
    var html = '';
    if (data != null) {
        $.each(data, function (i, item) {
            if (idlist.lastIndexOf('' + item.ID + '') >= 0) {
                html = html + '<option value="' + item.ID + '">' + item.Name + '</option>';
            }
        });
    }
    return html;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 20th Nov 2015
Description: Binding Drop Down By Passing Ids,Data,Control ID
---------------------------------------------------*/
function BindDropdownByIds(ids, data, controlId) {
    var idlist = [];
    if (ids != '')
        idlist = ids.split(',');
    var html = '';
    if (data != null) {
        $.each(data, function (i, item) {
            if (idlist.lastIndexOf('' + item.ID + '') >= 0) {
                html = html + '<option value="' + item.ID + '">' + item.Name + '</option>';
            }
        });
    }
    $('#' + controlId).html(html);
    //return html;
}


/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Getting Dropdown html bBy passing Data
---------------------------------------------------*/
function FormatHtml_Dropdown(data) {
    var _ddHtml = '';
    if (data != null) {
        $.each(data, function (i, item) {
            _ddHtml = _ddHtml + '<option value="' + item.ID + '">' + item.Name + '</option>';
        });
    }
    return _ddHtml;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Display Date as Calender Icon --2015-06-30
---------------------------------------------------*/

function DisplayDateasIcon(date) {
    //    var mdate = "2015-06-30";
    if (date != undefined && date != null && date != "") {
        var mdate = date;
        var dayThen = mdate.substr(8, 2);

        var monthThen = mdate.substr(5, 2);
        var yearThen = mdate.substr(0, 4);

        var html = '<div class="date"> <span class="month">' + GetMonthName(monthThen) + '</span> <span class="day">' + dayThen + '</span> <span class="year">' + yearThen + '</span></div>';
        return html;
    } else {
        return 'N/A';
    }

}
/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Display Date as Calender Icon --Day and MonthYear
---------------------------------------------------*/
function DisplayDateasIcon_DayandMonthYear(date) {
    //var mdate = "2015-06-30";
    var mdate = date;
    var dayThen = mdate.substr(8, 2);

    var monthThen = mdate.substr(5, 2);
    var yearThen = mdate.substr(0, 4);

    var html = '<div class="transactions-date-container"><div class="transactions-date">' + dayThen + '</div><div class="transactions-month">' + GetMonthName(monthThen) + ' ' + yearThen + '</div></div>';
    return html;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description:  Convert Undefined Value to N/A
---------------------------------------------------*/
function MakeNullasNotApplicable(value) {
    if (value == null || value == undefined) {
        return 'N/A';
    } else {
        return value;
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 31st Mar2016
Description:  Convert Empty Value or undefined or null to N/A
---------------------------------------------------*/
function MakeNEUasNotApplicable(value) {
    if (value == null || value == undefined || value == '') {
        return 'N/A';
    } else {
        return value;
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description:  Convert Undefined Value to N/A
---------------------------------------------------*/
function RoundUndefined(value) {
    if (value == undefined) {
        return 'N/A';
    } else {
        return value;
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Convert Undefined or Empty Value To null
---------------------------------------------------*/
function MakeNullfromUndefinedorEmpty(value) {
    if (value == undefined || value == "") {
        return null;
    } else {
        return value;
    }
}

function MakeEmptyfromUndefinedorNull(value) {
    if (value == undefined || value == null) {
        return '';
    } else {
        return value;
    }
}

function MakeZerofromUndefinedorEmpty(value) {
    if (value == undefined || value == "" || value == null) {
        return 0;
    } else {
        return value;
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function CheckSessionVariable(id) {
    if (id == "ErrorCode#1") {
        alert("Session Expired.Please Re-Login");
        window.location = "../../Account/MCareLogin";
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 19thAug2015
Description: Bind Results to Html Table Dynamically --B.SRINU 19thAug2015
---------------------------------------------------*/
function BindGridResults(data, tableId, count) {
    if (data != null) {
        $('#' + tableId).html('');
        var columns = [];
        if ((data != null)) {
            columns = GetColumnsOfJsonObj(data[0]);
            var tHead = "<thead><tr>";
            if (count == null || count == undefined || count == '')
                count = columns.length;
            count = (columns.length > count) ? count : columns.length;
            for (var i = 0; i < count; i++) {
                //  tHead = tHead + "<th>" + columns[i].capitalize() + "</th>";
                tHead = tHead + "<th>" + columns[i] + "</th>";
            }
            tHead = tHead + "</tr></thead>";
            $("#" + tableId).append(tHead);

            // Count = Count + data.length;
            for (var i = 0; i < data.length; i++) {

                if (data[i] != null) {
                    //$('#PreAuth_download').css({ 'display': 'inherit' });
                    var tr = "<tr class='tr'>";
                    for (var j = 0; j < count; j++) {
                        tr = tr + "<td>" + data[i][columns[j]] + "</td>";
                        //if (columns[j] == "Preauthid") {
                        //    tr = tr + '<td><a href="#" id="' + data[i][columns[j]] + '" onclick="PreauthStatus(this) ">' + data[i][columns[j]] + '</a></td>';
                        //} else {
                        //    tr = tr + "<td>" + data[i][columns[j]] + "</td>";
                        //}
                    }
                    tr = tr + "</tr>";
                    $("#" + tableId).append(tr);
                }
            }
        }
        if ((data.length == 0) || (data == null)) {
            // $('#PreAuth_download').css({ 'display': 'none' });
            var $tr = $('<tr class="tr">').append($('<td colspan="8" style=" font-size:large; color:#3A798C;text-align:center;">').text("No Records Found")
                                                 ).appendTo('#' + tableId);
        }
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 03rdOct2015
Description: Bind Results to Html Table Dynamically with Data Title for Responsive Table
---------------------------------------------------*/
function BindGridResultswithDataTitle(data, tableId, count) {
    if (data != null) {
        $('#' + tableId).html('');
        var columns = [];
        if ((data != null)) {
            columns = GetColumnsOfJsonObj(data[0]);
            var tHead = "<thead><tr>";
            if (count == null || count == undefined || count == '')
                count = columns.length;
            count = (columns.length > count) ? count : columns.length;
            for (var i = 0; i < count; i++) {
                //  tHead = tHead + "<th>" + columns[i].capitalize() + "</th>";
                tHead = tHead + "<th>" + columns[i] + "</th>";
            }
            tHead = tHead + "</tr></thead>";
            $("#" + tableId).append(tHead);

            // Count = Count + data.length;
            for (var i = 0; i < data.length; i++) {

                if (data[i] != null) {
                    //$('#PreAuth_download').css({ 'display': 'inherit' });
                    var tr = "<tr class='tr'>";
                    for (var j = 0; j < columns.length; j++) {
                        tr = tr + "<td data-title='" + columns[j] + "' >" + data[i][columns[j]] + "</td>";
                        //if (columns[j] == "Preauthid") {
                        //    tr = tr + '<td><a href="#" id="' + data[i][columns[j]] + '" onclick="PreauthStatus(this) ">' + data[i][columns[j]] + '</a></td>';
                        //} else {
                        //    tr = tr + "<td>" + data[i][columns[j]] + "</td>";
                        //}
                    }
                    tr = tr + "</tr>";
                    $("#" + tableId).append(tr);
                }
            }
        }
        if ((data.length == 0) || (data == null)) {
            // $('#PreAuth_download').css({ 'display': 'none' });
            var $tr = $('<tr class="tr">').append($('<td colspan="8" style=" font-size:large; color:#3A798C;text-align:center;">').text("No Records Found")
                                                 ).appendTo('#' + tableId);
        }
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 19thAug2015
Description: -----GET Column Names of Json Variable --B.SRINU 19thAug2015-------
---------------------------------------------------*/
function GetColumnsOfJsonObj(cols) {
    var columns = [];
    var j = 0;
    for (i in cols) {
        columns[j] = i;
        j++;
    }
    return columns;
}

/*-------------------------------------------------
Code Written By B. Srinu  on 19thAug2015
---------------------------------------------------*/
String.prototype.capitalize = function () {
    return this.substr(0, 1).toUpperCase() + this.substr(1).toLowerCase();
}


function popupCenter(url, title, w, h) {
    if ($('#hdnAppLive').val() == 'TRUE') {
        if ($('#hdnAppURL').val().toUpperCase().match('SPECTRA.FHPL.NET')) {
            //if ($('#hdnAppURL').val().toUpperCase().match('LOCALHOST')) {
            if (url.match('192.168.70.68')) {
                url = url.replace('192.168.70.68', 'Spectra.fhpl.net');
            }
            else if (url.match('192.168.70.73')) {
                url = url.replace('192.168.70.73', 'webshare.fhpl.net');
            }
        }
    }
    else {
        if ($('#hdnAppURL').val().toUpperCase().match('223.30.105.165')) {
            url = url.replace('200.200.201.173', '223.30.105.165');
        }
    }

    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

/*-------------------------------------------------
Code Written By B. Srinu  on 19thAug2015
---------------------------------------------------*/
function htmlEncode(html) {
    return document.createElement('a').appendChild(
        document.createTextNode(html)).parentNode.innerHTML;
};
/*-------------------------------------------------
Code Written By B. Srinu  on 19thAug2015
---------------------------------------------------*/
function htmlDecode(html) {
    var a = document.createElement('a'); a.innerHTML = html;
    return a.textContent;
};
/*-------------------------------------------------
Code Written By B. Srinu  on 8thOct2015
//Converting True/ False values as Yes/no
---------------------------------------------------*/

function ConvertTFtoYN(val) {
    var _res = 'N/A';

    if (val == null || val == undefined) {
        return _res;
    } else {
        return val == true ? 'Yes' : 'No';
    }
}

//---------------------------------------------------------------------Module Specific Methods------------------------------Starts Here

//  ............. TAT DETAILS Start Here...................

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Enrollment TAT
---------------------------------------------------*/
//function TatValidation(type, EntityID) {
//    var valid = false;
//    if (EntityID == '' || EntityID == 0) {
//        DialogWarningMessage('Please Get Entity Details');
//        return valid;
//    }
//    else if ($('#ddl' + type + 'TatLevel').val() == '') {
//        DialogWarningMessage('Please Select Tat Level');
//        return valid;
//    } else if ($('#txt' + type + 'TatDuration').val() == '') {
//        DialogWarningMessage('Please Enter Duration');
//        return valid;
//    }
//        //else if ($('#txt' + type + 'TatDuration').val() > 24 ) {
//        //    alert('Please Enter Tat In Days ');
//        //    return valid;
//        //}
//    else if ($('#ddl' + type + 'TatDurationType').val() == '') {
//        DialogWarningMessage('Please Select Duration Type');
//        return valid;
//    } else if ($('#txt' + type + 'TatEffctDate').val() == '') {
//        DialogWarningMessage('Please Enter Effective Date');
//        return valid;
//    }
//    return true;
//}

function TatValidation(type, EntityID, EffectiveDate) {
    //alert($('#txt' + type + 'TatEffctDate').val());
    //alert(JSONDate3(EffectiveDate));

    //alert(EffectiveDate);
    //alert(EffectiveDate > $('#txt' + type + 'TatEffctDate').val());
    //var a = EffectiveDate.trim();
    //var b = $('#txt' + type + 'TatEffctDate').val();

    //alert(b);
    //alert(a>b);
    var valid = false;
    if (EntityID == '' || EntityID == 0) {
        DialogWarningMessage('Please Get Entity Details');
        return valid;
    }
    else if ($('#ddl' + type + 'TatLevel').val() == '') {
        DialogWarningMessage('Please Select Tat Level');
        return valid;
    } else if ($('#txt' + type + 'TatDuration').val() == '') {
        DialogWarningMessage('Please Enter Duration');
        return valid;
    }
        //else if ($('#txt' + type + 'TatDuration').val() > 24 ) {
        //    alert('Please Enter Tat In Days ');
        //    return valid;
        //}
    else if ($('#ddl' + type + 'TatDurationType').val() == '') {
        DialogWarningMessage('Please Select Duration Type');
        return valid;
    } else if ($('#txt' + type + 'TatEffctDate').val() == '') {
        DialogWarningMessage('Please Enter Effective Date');
        return valid;
    }
    else if (Date.parse(JSONDate3(EffectiveDate)) > Date.parse(JSONDate3($('#txt' + type + 'TatEffctDate').val()))) {
        DialogWarningMessage('Enter TAT Effective Date Morethan or Equal to  Effective Date (' + EffectiveDate + ')');
        return valid;
    }
    //else if (Date.parse(JSONDate3(EffectiveDate)) > Date.parse(JSONDate3($('#txt' + type + 'TatEffctDate').val()))) {
    return true;
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function LoadEnrollmentTatData(data, EntityID, URL) {
    if (data != null) {
        $('#tblEnrollmentTatLevel tbody').html('');
        $('#ddlEnrollTatLevel option:gt(0)').remove();
        BindDropdown(MasterData["Mst_EnrollmentTATLevel"], "ddlEnrollTatLevel");

        $.each(data, function (i, item) {

            if ($('#hdnActType').val() == 1) {
                $('#ddlEnrollTatLevel option').each(function () {
                    if (item.TATLevelID == this.value) {
                        $('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + item.TATLevelID + '"><td>' + $('#ddlEnrollTatLevel option[value="' + item.TATLevelID + '"]').text()
                       + '</td><td>' + item.Duration + '</td><td>' + $('#ddlEnrollTatDurationType option[value="' + item.DurationTypeID + '"]').text() + '</td><td>'
                           + JSONDate2(item.EffectiveDate) + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick"  ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" ><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');

                        $('#ddlEnrollTatLevel option[value=' + item.TATLevelID + ']').remove();
                        $('#tblEnrollmentTatLevel').show('');
                    }

                });
            } else {
                $('#ddlEnrollTatLevel option').each(function () {
                    if (item.TATLevelID == this.value) {
                        $('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + item.TATLevelID + '"><td>' + $('#ddlEnrollTatLevel option[value="' + item.TATLevelID + '"]').text()
                       + '</td><td>' + item.Duration + '</td><td>' + $('#ddlEnrollTatDurationType option[value="' + item.DurationTypeID + '"]').text() + '</td><td>'
                           + JSONDate2(item.EffectiveDate) + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditEnrollmentTatLevel(' + item.TATLevelID + ',\''
                           + $('#ddlEnrollTatLevel option[value="' + item.TATLevelID + '"]').text() + '\',' + item.Duration + ',' + item.DurationTypeID + ',\'' + JSONDate2(item.EffectiveDate)
                       + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemoveEnrollmentTatLevel('
                       + item.TATLevelID + ',\'' + $('#ddlEnrollTatLevel option[value="' + item.TATLevelID + '"]').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');

                        $('#ddlEnrollTatLevel option[value=' + item.TATLevelID + ']').remove();
                        $('#tblEnrollmentTatLevel').show('');
                    }

                });
            }
        });
    }
    //  $('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + ddlEnrollTatLevel.value + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + txtEnrollTatDuration.value + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" onclick="EditEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + txtEnrollTatDuration.value + ',' + ddlEnrollTatDurationType.value + ',\'' + txtEnrollTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" onclick="RemoveEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function AddEnrollmentTatLevel(URL, EntityID, EffectiveDate) {

    if (TatValidation('Enroll', EntityID, EffectiveDate)) {

        if (EntityID != '') {
            ajaxGETResonse("/" + URL + "/AddTATConfig", function (res) {
                //ajaxGETResonse("/AddTATConfig", function (res) {
                //  alert(res);
                if ($('#tr' + ddlEnrollTatLevel.value).length == 0) {
                    $('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + ddlEnrollTatLevel.value + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + $('#txtEnrollTatDuration').val() + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + txtEnrollTatDuration.value + ',' + ddlEnrollTatDurationType.value + ',\'' + txtEnrollTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemoveEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                } else {
                    $('#tr' + ddlEnrollTatLevel.value).html('<td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + $('#txtEnrollTatDuration').val() + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + txtEnrollTatDuration.value + ',' + ddlEnrollTatDurationType.value + ',\'' + txtEnrollTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemoveEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td>');
                }
                $('#ddlEnrollTatLevel option[value=' + ddlEnrollTatLevel.value + ']').remove();
                if ($('#hdnenrollTatedititem').val() != "") {
                    $('#ddlEnrollTatLevel option[value=' + $('#hdnenrollTatedititem').val() + ']').remove();
                    $('#hdnenrollTatedititem').val('')
                }

                ddlEnrollTatLevel.value = '';
                $('#txtEnrollTatDuration').val('');
                ddlEnrollTatDurationType.value = '';
                txtEnrollTatEffctDate.value = '';
                $('#tblEnrollmentTatLevel').show('');
                DialogResultMessage('Enrollment TAT Details Successfully Inserted');
            }, errorfun, { values: JSON.stringify(consolidateElements('divEnrollmentTat')), entityID: EntityID })
        }


        //$('#ddlEnrollTatLevel').val('');

    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function RemoveEnrollmentTatLevel(tatlevelid, tattext, EntityID, URL) {
    if (EntityID != '') {

        if ($('#hdnenrollTatedititem').val() == tatlevelid) {
            //$('#ddlEnrollTatLevel option[value=' + $('#hdnenrollTatedititem').val() + ']').remove();
            DialogWarningMessage('Tat in Edit Mode');
        } else {
            $('#tr' + tatlevelid).remove();
            $('#ddlEnrollTatLevel').append('<option value=' + tatlevelid + '>' + tattext + '</option>');
            ajaxGETResonse("/" + URL + "/RemoveTATConfig", function (res) {
                // alert(res);

                //$('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + res + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + txtEnrollTatDuration.value + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" onclick="UpdateEnrollmentTatLevel(' + res + ');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" onclick="RemoveEnrollmentTatLevel(' + res + ');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                //  $('#ddlEnrollTatLevel option[value=' + ddlEnrollTatLevel.value + ']').remove();
                // ddlEnrollTatLevel.value = '';
                // txtEnrollTatDuration.value = '';
                // ddlEnrollTatDurationType.value = '';
                //txtEnrollTatEffctDate.value = '';
                DialogResultMessage('Enrollment TAT Details Successfully Removed');
            }, errorfun, { ID: tatlevelid, entityID: EntityID })
        }
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function EditEnrollmentTatLevel(tid, tval, duration, durtype, TatEffctDate) {
    if ($('#hdnenrollTatedititem').val() != "") {
        $('#ddlEnrollTatLevel option[value=' + $('#hdnenrollTatedititem').val() + ']').remove();
    }
    $('#hdnenrollTatedititem').val(tid);

    if ($('#ddlEnrollTatLevel option[value=' + tid + ']').length == 0) {
        $('#ddlEnrollTatLevel').append('<option value=' + tid + '>' + tval + '</option>');
    }

    //$('#ddlEnrollTatLevel:eq(0) option').each(function () {
    //    alert($(this).val() + ' :: ' + $(this).text());
    //});

    //$('#ddlEnrollTatLevel option:gt(0)').each(function () {
    //    alert($(this).val() + ' :: ' + $(this).text());
    //});

    //  $('#tr' + tid).attr('class', 'active');
    ddlEnrollTatLevel.value = tid;
    txtEnrollTatDuration.value = duration;
    ddlEnrollTatDurationType.value = durtype;
    // txtEnrollTatEffctDate.value = edate;
    JSONDate2WithCntrl(TatEffctDate, "txtEnrollTatEffctDate");
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
// Preauth TAT
---------------------------------------------------*/

function LoadPreauthTatData(data, EntityID, URL) {
    if (data != null) {
        $('#tblPreauthTatLevel tbody').html('');
        $('#ddlPreauthTatLevel option:gt(0)').remove();
        BindDropdown(MasterData["Mst_PreauthTATLevel"], "ddlPreauthTatLevel");

        $.each(data, function (i, item) {
            if ($('#hdnActType').val() == 1) {
                $('#ddlPreauthTatLevel option').each(function () {
                    if (item.TATLevelID == this.value) {
                        $('#tblPreauthTatLevel tbody').append('<tr id="tr' + item.TATLevelID + '"><td>' + $('#ddlPreauthTatLevel option[value="' + item.TATLevelID + '"]').text() + '</td><td>' + item.Duration + '</td><td>' + $('#ddlPreauthTatDurationType option[value="' + item.DurationTypeID + '"]').text() + '</td><td>' + JSONDate2(item.EffectiveDate) + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick"  ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" ><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                        $('#ddlPreauthTatLevel option[value=' + item.TATLevelID + ']').remove();
                        $('#tblPreauthTatLevel').show('');
                    }

                });
            } else {
                $('#ddlPreauthTatLevel option').each(function () {
                    if (item.TATLevelID == this.value) {
                        $('#tblPreauthTatLevel tbody').append('<tr id="tr' + item.TATLevelID + '"><td>' + $('#ddlPreauthTatLevel option[value="' + item.TATLevelID + '"]').text() + '</td><td>' + item.Duration + '</td><td>' + $('#ddlPreauthTatDurationType option[value="' + item.DurationTypeID + '"]').text() + '</td><td>' + JSONDate2(item.EffectiveDate) + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditPreauthTatLevel(' + item.TATLevelID + ',\'' + $('#ddlPreauthTatLevel option[value="' + item.TATLevelID + '"]').text() + '\',' + item.Duration + ',' + item.DurationTypeID + ',\'' + JSONDate2(item.EffectiveDate) + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemovePreauthTatLevel(' + item.TATLevelID + ',\'' + $('#ddlPreauthTatLevel option[value="' + item.TATLevelID + '"]').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                        $('#ddlPreauthTatLevel option[value=' + item.TATLevelID + ']').remove();
                        $('#tblPreauthTatLevel').show('');
                    }
                });
            }
        });
    }
    //  $('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + ddlEnrollTatLevel.value + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + txtEnrollTatDuration.value + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" onclick="EditEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + txtEnrollTatDuration.value + ',' + ddlEnrollTatDurationType.value + ',\'' + txtEnrollTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" onclick="RemoveEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function AddPreauthTatLevel(URL, EntityID, EffectiveDate) {
    if (TatValidation('Preauth', EntityID, EffectiveDate)) {

        if (EntityID != '') {
            ajaxGETResonse("/" + URL + "/AddPreauthTATConfig", function (res) {
                //  alert(res);
                if ($('#tr' + ddlPreauthTatLevel.value).length == 0) {
                    $('#tblPreauthTatLevel tbody').append('<tr id="tr' + ddlPreauthTatLevel.value + '"><td>' + $('#ddlPreauthTatLevel option:selected').text() + '</td><td>' + txtPreauthTatDuration.value + '</td><td>' + $('#ddlPreauthTatDurationType option:selected').text() + '</td><td>' + txtPreauthTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditPreauthTatLevel(' + ddlPreauthTatLevel.value + ',\'' + $('#ddlPreauthTatLevel option:selected').text() + '\',' + txtPreauthTatDuration.value + ',' + ddlPreauthTatDurationType.value + ',\'' + txtPreauthTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemovePreauthTatLevel(' + ddlPreauthTatLevel.value + ',\'' + $('#ddlPreauthTatLevel option:selected').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                } else {
                    $('#tr' + ddlPreauthTatLevel.value).html('<td>' + $('#ddlPreauthTatLevel option:selected').text() + '</td><td>' + txtPreauthTatDuration.value + '</td><td>' + $('#ddlPreauthTatDurationType option:selected').text() + '</td><td>' + txtPreauthTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditPreauthTatLevel(' + ddlPreauthTatLevel.value + ',\'' + $('#ddlPreauthTatLevel option:selected').text() + '\',' + txtPreauthTatDuration.value + ',' + ddlPreauthTatDurationType.value + ',\'' + txtPreauthTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemovePreauthTatLevel(' + ddlPreauthTatLevel.value + ',\'' + $('#ddlPreauthTatLevel option:selected').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td>');
                }
                $('#ddlPreauthTatLevel option[value=' + ddlPreauthTatLevel.value + ']').remove();
                if ($('#hdnPreauthTatedititem').val() != "") {
                    $('#ddlPreauthTatLevel option[value=' + $('#hdnPreauthTatedititem').val() + ']').remove();
                    $('#hdnPreauthTatedititem').val('');
                }
                ddlPreauthTatLevel.value = '';
                txtPreauthTatDuration.value = '';
                ddlPreauthTatDurationType.value = '';
                txtPreauthTatEffctDate.value = '';
                $('#tblPreauthTatLevel').show('');
                DialogResultMessage('Preauth TAT Details Successfully Inserted');
            }, errorfun, { values: JSON.stringify(consolidateElements('divPreauthTat')), entityID: EntityID })
        }


        //$('#ddlEnrollTatLevel').val('');

    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function RemovePreauthTatLevel(tatlevelid, tattext, EntityID, URL) {

    if (EntityID != '') {

        if ($('#hdnPreauthTatedititem').val() == tatlevelid) {
            //$('#ddlEnrollTatLevel option[value=' + $('#hdnenrollTatedititem').val() + ']').remove();
            DialogWarningMessage('Tat in Edit Mode');
        } else {
            $('#tr' + tatlevelid).remove();
            $('#ddlPreauthTatLevel').append('<option value=' + tatlevelid + '>' + tattext + '</option>');
            ajaxGETResonse("/" + URL + "/RemovePreauthTATConfig", function (res) {
                // alert(res);

                //$('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + res + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + txtEnrollTatDuration.value + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" onclick="UpdateEnrollmentTatLevel(' + res + ');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" onclick="RemoveEnrollmentTatLevel(' + res + ');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                //  $('#ddlEnrollTatLevel option[value=' + ddlEnrollTatLevel.value + ']').remove();
                // ddlEnrollTatLevel.value = '';
                // txtEnrollTatDuration.value = '';
                // ddlEnrollTatDurationType.value = '';
                //txtEnrollTatEffctDate.value = '';
                DialogResultMessage('Preauth TAT Details Successfully Removed');
            }, errorfun, { ID: tatlevelid, entityID: EntityID })
        }
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function EditPreauthTatLevel(tid, tval, duration, durtype, TatEffctDate) {
    if ($('#hdnPreauthTatedititem').val() != "") {
        $('#ddlPreauthTatLevel option[value=' + $('#hdnPreauthTatedititem').val() + ']').remove();
    }
    $('#hdnPreauthTatedititem').val(tid);

    if ($('#ddlPreauthTatLevel option[value=' + tid + ']').length == 0) {
        $('#ddlPreauthTatLevel').append('<option value=' + tid + '>' + tval + '</option>');
    }

    //$('#ddlEnrollTatLevel:eq(0) option').each(function () {
    //    alert($(this).val() + ' :: ' + $(this).text());
    //});

    //$('#ddlEnrollTatLevel option:gt(0)').each(function () {
    //    alert($(this).val() + ' :: ' + $(this).text());
    //});

    //  $('#tr' + tid).attr('class', 'active');
    ddlPreauthTatLevel.value = tid;
    txtPreauthTatDuration.value = duration;
    ddlPreauthTatDurationType.value = durtype;
    // txtEnrollTatEffctDate.value = edate;
    JSONDate2WithCntrl(TatEffctDate, "txtPreauthTatEffctDate");
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Claims TAT
---------------------------------------------------*/

function LoadClaimsTatData(data, EntityID, URL) {
    if (data != null) {
        $('#tblClaimsTatLevel tbody').html('');
        $('#ddlClaimsTatLevel option:gt(0)').remove();
        BindDropdown(MasterData["Mst_ClaimsTATLevel"], "ddlClaimsTatLevel");
        //  alert(hdnActType.value);
        if ($('#hdnActType').val() == 1) {
            $.each(data, function (i, item) {
                $('#ddlClaimsTatLevel option').each(function () {
                    if (item.TATLevelID == this.value) {
                        $('#tblClaimsTatLevel tbody').append('<tr id="tr' + item.TATLevelID + '"><td>' + $('#ddlClaimsTatLevel option[value="' + item.TATLevelID + '"]').text() + '</td><td>' + item.Duration + '</td><td>' + $('#ddlClaimsTatDurationType option[value="' + item.DurationTypeID + '"]').text() + '</td><td>' + JSONDate2(item.EffectiveDate) + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick"  ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" ><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                        $('#ddlClaimsTatLevel option[value=' + item.TATLevelID + ']').remove();
                        $('#tblClaimsTatLevel').show('');
                    }
                });
            });
        } else {
            $.each(data, function (i, item) {

                $('#ddlClaimsTatLevel option').each(function () {
                    if (item.TATLevelID == this.value) {
                        $('#tblClaimsTatLevel tbody').append('<tr id="tr' + item.TATLevelID + '"><td>' + $('#ddlClaimsTatLevel option[value="' + item.TATLevelID + '"]').text() + '</td><td>' + item.Duration + '</td><td>' + $('#ddlClaimsTatDurationType option[value="' + item.DurationTypeID + '"]').text() + '</td><td>' + JSONDate2(item.EffectiveDate) + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditClaimsTatLevel(' + item.TATLevelID + ',\'' + $('#ddlClaimsTatLevel option[value="' + item.TATLevelID + '"]').text() + '\',' + item.Duration + ',' + item.DurationTypeID + ',\'' + JSONDate2(item.EffectiveDate) + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemoveClaimsTatLevel(' + item.TATLevelID + ',\'' + $('#ddlClaimsTatLevel option[value="' + item.TATLevelID + '"]').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                        $('#ddlClaimsTatLevel option[value=' + item.TATLevelID + ']').remove();
                        $('#tblClaimsTatLevel').show('');
                    }
                });
            });
        }
    }
    //  $('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + ddlEnrollTatLevel.value + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + txtEnrollTatDuration.value + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" onclick="EditEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\',' + txtEnrollTatDuration.value + ',' + ddlEnrollTatDurationType.value + ',\'' + txtEnrollTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" onclick="RemoveEnrollmentTatLevel(' + ddlEnrollTatLevel.value + ',\'' + $('#ddlEnrollTatLevel option:selected').text() + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function AddClaimsTatLevel(URL, EntityID, EffectiveDate) {
    if (TatValidation('Claims', EntityID, EffectiveDate)) {
        if (EntityID != '') {
            ajaxGETResonse("/" + URL + "/AddClaimsTATConfig", function (res) {
                //  alert(res);
                if ($('#tr' + ddlClaimsTatLevel.value).length == 0) {
                    $('#tblClaimsTatLevel tbody').append('<tr id="tr' + ddlClaimsTatLevel.value + '"><td>' + $('#ddlClaimsTatLevel option:selected').text() + '</td><td>' + txtClaimsTatDuration.value + '</td><td>' + $('#ddlClaimsTatDurationType option:selected').text() + '</td><td>' + txtClaimsTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditClaimsTatLevel(' + ddlClaimsTatLevel.value + ',\'' + $('#ddlClaimsTatLevel option:selected').text() + '\',' + txtClaimsTatDuration.value + ',' + ddlClaimsTatDurationType.value + ',\'' + txtClaimsTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemoveClaimsTatLevel(' + ddlClaimsTatLevel.value + ',\'' + $('#ddlClaimsTatLevel option:selected').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                } else {
                    $('#tr' + ddlClaimsTatLevel.value).html('<td>' + $('#ddlClaimsTatLevel option:selected').text() + '</td><td>' + txtClaimsTatDuration.value + '</td><td>' + $('#ddlClaimsTatDurationType option:selected').text() + '</td><td>' + txtClaimsTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" name="TATbtnclick" onclick="EditClaimsTatLevel(' + ddlClaimsTatLevel.value + ',\'' + $('#ddlClaimsTatLevel option:selected').text() + '\',' + txtClaimsTatDuration.value + ',' + ddlClaimsTatDurationType.value + ',\'' + txtClaimsTatEffctDate.value + '\');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" name="TATbtnclick" onclick="RemoveClaimsTatLevel(' + ddlClaimsTatLevel.value + ',\'' + $('#ddlClaimsTatLevel option:selected').text() + '\',' + EntityID + ',\'' + URL + '\');"><span class="glyphicon glyphicon-trash"></span></a>  </td>');
                }
                $('#ddlClaimsTatLevel option[value=' + ddlClaimsTatLevel.value + ']').remove();
                if ($('#hdnClaimsTatedititem').val() != "") {
                    $('#ddlClaimsTatLevel option[value=' + $('#hdnClaimsTatedititem').val() + ']').remove();
                    $('#hdnClaimsTatedititem').val('')
                }

                ddlClaimsTatLevel.value = '';
                txtClaimsTatDuration.value = '';
                ddlClaimsTatDurationType.value = '';
                txtClaimsTatEffctDate.value = '';
                $('#tblClaimsTatLevel').show('');
                DialogResultMessage('Claim TAT Details Successfully Inserted');
            }, errorfun, { values: JSON.stringify(consolidateElements('divClaimsTat')), entityID: EntityID })
        }
        //$('#ddlEnrollTatLevel').val('');
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function RemoveClaimsTatLevel(tatlevelid, tattext, EntityID, URL) {

    if (EntityID != '') {

        if ($('#hdnClaimsTatedititem').val() == tatlevelid) {
            //$('#ddlEnrollTatLevel option[value=' + $('#hdnenrollTatedititem').val() + ']').remove();
            DialogWarningMessage('Tat in Edit Mode');
        } else {
            $('#tr' + tatlevelid).remove();
            $('#ddlClaimsTatLevel').append('<option value=' + tatlevelid + '>' + tattext + '</option>');
            ajaxGETResonse("/" + URL + "/RemoveClaimsTATConfig", function (res) {
                // alert(res);

                //$('#tblEnrollmentTatLevel tbody').append('<tr id="tr' + res + '"><td>' + $('#ddlEnrollTatLevel option:selected').text() + '</td><td>' + txtEnrollTatDuration.value + '</td><td>' + $('#ddlEnrollTatDurationType option:selected').text() + '</td><td>' + txtEnrollTatEffctDate.value + '</td><td><a class="btn btn-info btn-xs" onclick="UpdateEnrollmentTatLevel(' + res + ');" ><span class="glyphicon glyphicon-pencil"></span></a>     <a class="btn btn-danger btn-xs" onclick="RemoveEnrollmentTatLevel(' + res + ');"><span class="glyphicon glyphicon-trash"></span></a>  </td></tr>');
                //  $('#ddlEnrollTatLevel option[value=' + ddlEnrollTatLevel.value + ']').remove();
                // ddlEnrollTatLevel.value = '';
                // txtEnrollTatDuration.value = '';
                // ddlEnrollTatDurationType.value = '';
                //txtEnrollTatEffctDate.value = '';
                DialogResultMessage('Claims TAT Details Successfully Removed');
            }, errorfun, { ID: tatlevelid, entityID: EntityID })
        }
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function EditClaimsTatLevel(tid, tval, duration, durtype, TatEffctDate) {
    if ($('#hdnClaimsTatedititem').val() != "") {
        $('#ddlClaimsTatLevel option[value=' + $('#hdnClaimsTatedititem').val() + ']').remove();
    }
    $('#hdnClaimsTatedititem').val(tid);

    if ($('#ddlClaimsTatLevel option[value=' + tid + ']').length == 0) {
        $('#ddlClaimsTatLevel').append('<option value=' + tid + '>' + tval + '</option>');
    }

    //$('#ddlEnrollTatLevel:eq(0) option').each(function () {
    //    alert($(this).val() + ' :: ' + $(this).text());
    //});

    //$('#ddlEnrollTatLevel option:gt(0)').each(function () {
    //    alert($(this).val() + ' :: ' + $(this).text());
    //});

    //  $('#tr' + tid).attr('class', 'active');
    ddlClaimsTatLevel.value = tid;
    txtClaimsTatDuration.value = duration;
    ddlClaimsTatDurationType.value = durtype;
    // txtEnrollTatEffctDate.value = edate;
    JSONDate2WithCntrl(TatEffctDate, "txtClaimsTatEffctDate");
}


//  ............. TAT DETAILS End Here...................

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetBankDetails() {
    //  $('#IFSCCode').on("change", function () {
    if ($('#txtIFSCCode').val() != '') {
        $.ajax({
            //url: 'Corporate/GetBankDetails',
            url: '/Common/GetBankDetails',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data:
                { IFSC: $('#txtIFSCCode').val() },

            dataType: 'json',
            success: function (data) {
                // alert(data);
                if (data != null) {
                    $('#txtBankName').val(data.Name);
                    $('#txtBranchName').val(data.Branch);
                    $('#txtBankAddress').val(data.Address);
                    $('#txtMICRCode').val(data.MICRCode);
                    $('#hdnBankId').val(data.ID);


                } else {
                    DialogWarningMessage("Invalid IFSC Code");
                }

            },
            error: function (err) {
                alert(err);
                //err[0]
            }
        });
        //  });
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on  
Description : File Upload 
---------------------------------------------------*/
//
function UploadFile(type, id) {
    if (id == '' || id == null || id == 'undefined' || id == 0) {
        DialogWarningMessage('Please Enter or Select Entity Type');
    } else {
        var files = $("#fileUpload").get(0).files;
        if (files.length != 0) {
            var data = new FormData();



            // Add the uploaded image content to the form data collection
            if (files.length > 0) {
                data.append("UploadedImage", files[0]);
            }

            // Make Ajax request with the contentType = false, and procesDate = false
            var ajaxRequest = $.ajax({
                type: "POST",
                url: "/FileUpload/UploadFile?EntityType=" + type + '&EntityId=' + id,
                contentType: false,
                processData: false,
                data: data
            });

            ajaxRequest.done(function (xhr, textStatus) {
                // Do other operation
                // GetDocuments(type, id);
                GetDocuments(type, id);
                DialogResultMessage('File uploaded Successfully');
            });
        } else {
            DialogWarningMessage('Please Select File to Upload');
        }
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
Description: Multiple File Upload 
---------------------------------------------------*/
function MultipleUploadFile(type, id) {
    if (id == '' || id == null || id == 'undefined') {
        DialogWarningMessage('Please Enter or Select Entity Type');
    } else {
        var files = $("#fileUpload").get(0).files;
        if (files.length != 0) {

            $.each(files, function (i, item) {


                var data = new FormData();



                // Add the uploaded image content to the form data collection
                if (files.length > 0) {
                    data.append("UploadedImage", files[i]);
                }

                // Make Ajax request with the contentType = false, and procesDate = false
                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "/FileUpload/UploadFile?EntityType=" + type + '&EntityId=' + id,
                    contentType: false,
                    processData: false,
                    data: data
                });
                ajaxRequest.done(function (xhr, textStatus) {
                    // Do other operation
                    // GetDocuments(type, id);

                });
            });
            //GetDocuments(type, id);
            DialogResultMessage('Files uploaded Successfully');

        } else {
            DialogWarningMessage('Please Select File to Upload');
        }
    }
}
/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetDocuments(type, id) {
    if (id == '' || id == null || id == 'undefined') {
        DialogWarningMessage('Please Enter or Select Entity Type');
    } else {

        $.ajax({
            url: '/FileUpload/GetDocuments',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data:
                { EntityType: type, EntityId: id },

            dataType: 'json',
            success: function (data) {

                // alert("Hi");
                // alert(data[0]);


                if (data != null) {
                    var str = "<ul>";
                    $.each(data, function (i, item) {
                        //alert(item);
                        var vals = item.split('^');
                        vals[2] = vals[2].split('.')[1];
                        //str = str + "<li> <a  href='FileUpload/GetFile/" + vals[1] + "_" + vals[2] + "_" + vals[0].split('.')[0] + "' >" + vals[0] + "</a>" + "<a class='btn btn-danger btn-xs'  onclick='RemoveFile(\"" + vals[1] + "\");' ><span class='glyphicon glyphicon-trash'></span></a></li>";
                        str = str + "<li> <a  href='/FileUpload/GetFile/" + vals[1] + "_" + vals[2] + "_" + vals[0].split('.')[0] + "' >" + vals[0] + "</a></li>";
                    })
                    //alert(str);
                    $('#divDocumentList').html(str + '</ul>');

                } else {
                    //   alert("Records Not ");
                }

            },
            error: function (err) {
                alert(err);
                //alert("Hello");
                //err[0]
            }
        });

        // ajaxGETResonse("/FileUpload/GetDocuments", function (res) { alert(res); }, errorfun, { EntityType: type, EntityId: id });

    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
function GetFile(id) {
    if (id == '' || id == null || id == 'undefined') {
        DialogWarningMessage('Please Enter or Select Entity Type');
    } else {

        $.ajax({
            url: 'FileUpload/GetFile',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data:
                { ID: id },

            //dataType: 'json',
            success: function (data) {

                // alert(data);
                // alert(data[0]);


                //if (data != null) {
                //    var str = "<ul>";
                //    $.each(data, function (i, item) {
                //        str = str + "<li> <a href='' target='_blank'>" + item + "</a></li>";
                //    })
                //    //alert(str);
                //    $('#divDocumentList').html(str + '</ul>');

                //} else {
                //    //   alert("Records Not ");
                //}

            },
            error: function (err) {
                // alert(err);
                alert("Hello");
                //err[0]
            }
        });

        // ajaxGETResonse("/FileUpload/GetDocuments", function (res) { alert(res); }, errorfun, { EntityType: type, EntityId: id });

    }
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Get next action items by department
---------------------------------------------------*/
function GetNextactions(deptId, ActivityID) {
    if (deptId != '') {
        $.ajax({
            url: '/Common/GetNextActions',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            data:
                { DeptID: deptId },

            dataType: 'json',
            success: function (data) {
                if (data != null) {
                    BindDropdown(data, ActivityID);
                    //$("#" + ActivityID + " option:gt(0)").remove();
                    //$.each(data, function (i) {
                    //    var optionhtml = '<option value="' +
                    //data[i].ID + '">' + data[i].Name + '</option>';
                    //    $("#" + ActivityID).append(optionhtml);
                    //});

                } else {
                    DialogWarningMessage("No Role Found to this Department");
                }

            },
            error: function (err) {
                alert(err);
                //err[0]
            }
        });
    }
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Common confirm dialog for form submission by passing dialog content and title
---------------------------------------------------*/
function CommonConfirmDialog(e, innertext, titletext) {
    e.preventDefault();
    $('#dialogInnerText').text(innertext);
    $("#dialog-confirm").removeClass('hide').dialog({
        resizable: false,
        width: '320',
        modal: true,
        title: titletext,
        //title:"<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Confirm",
                "class": "btn btn-danger btn-minier",
                click: function () {
                    $(this).dialog("close");
                    $('form').submit();
                    return true;

                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                    e.preventDefault();
                }
            }
        ]
    });
}


/*-------------------------------------------------
Code Written By B. Srinu  on 28-Nov-2015
Description : Common dialog for Showing Div by passing dialog content html,title ,width and Height
---------------------------------------------------*/
function CommonDialog(innerhtml, titletext, successmethod, closemethod, _width, _height) {
    //e.preventDefault();
    $('#divCommonalertmessage').html('');
    $('#dialogcommonInnerText').html(innerhtml);
    $("#dialog-common").removeClass('hide').dialog({
        resizable: false,
        width: _width,
        height: _height,
        modal: true,
        title: titletext,
        //title:"<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
        title_html: true,
        buttons: [
            //{
            //    html: "&nbsp; OK",//"<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Proceed",
            //    "class": "btn btn-danger btn-minier",
            //    click: function () {
            //        $(this).dialog("close");
            //        if (successmethod != null) successmethod();
            //        return true;

            //    }
            //}
            //,
            {
                html: "<i class='ace-icon fa fa-times bigger-110' id='btncommondialogclose'></i>&nbsp; Close",
                "class": "btn btn-minier",
                click: function () {
                    if (closemethod != null && closemethod != '')
                        closemethod();
                    $(this).dialog("close");
                    return false;
                }
            }
        ]
    });
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Common confirm dialog for ajax calling request submission by passing dialog content and title
---------------------------------------------------*/
function CommonConfirmAjaxDialog(innertext, titletext, successmethod, errormethod) {
    //e.preventDefault();
    $('#dialogInnerText').text(innertext);
    $("#dialog-confirm").removeClass('hide').dialog({
        resizable: false,
        width: '320',
        modal: true,
        title: titletext,
        //title:"<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='ace-icon fa fa-check-o bigger-110'></i>&nbsp; Confirm",
                "class": "btn btn-danger btn-minier",
                click: function () {
                    $(this).dialog("close");
                    if (successmethod != null) successmethod();
                    return true;

                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                    if (errormethod != null) errormethod();
                    return false;
                }
            }
        ]
    });
}


/*-------------------------------------------------
Code Written By Nagaraju
---------------------------------------------------*/
function GetUserName(id, regions, users) {
    var uID = '';
    if (regions != null) {
        $.each(regions, function (i, item) {
            if (item.ID == id) {
                uID = item.UserID;
            }
        });
    }

    var uName = '';
    $.each(users, function (i, item) {
        if (item.ID == id) {
            uName = item.Name;
        }
    });

    return uName;
}

/*-------------------------------------------------
Code Written By Nagaraju
---------------------------------------------------*/
function Cascading_OnlyDistricts(ctrlDistrict, ID) {
    $("#" + ctrlDistrict + " option:gt(0)").remove();
    var optionhtml = '<option value=0>Select</option>';
    var values = "";
    // Bind Districts
    values = MasterData.Mst_District["Result"];
    $.each(values, function (i) {
        if (ID == values[i].StateID) {
            optionhtml = '<option value="' + values[i].ID + '">' + values[i].Name + '</option>';
            $("#" + ctrlDistrict).append(optionhtml);
        }
    });
}

/*-------------------------------------------------
Code Written By Nagaraju
---------------------------------------------------*/
function BindServiceType(_serviceTypeID, ctrl) {
    $("#" + ctrl + " :gt(0)").remove();
    var serviceType = MasterData.mClaimServiceType["Result"];
    $.each(serviceType, function (i, item) {
        var id = 0;
        if (item.ParentID == _serviceTypeID) {
            var optionhtml = '<option value="' + serviceType[i].ID + '">' + serviceType[i].Name + '</option>';
            $("#" + ctrl).append(optionhtml);
        }
    });
}

/*-------------------------------------------------
Code Written By Nagaraju
---------------------------------------------------*/
function BindCheckBox(_value, ctrl) {
    if (_value == 1 || _value == true)
        $("#" + ctrl).attr("checked", true);
}



//Get the  Date(30-Nov-1990) for Manual Entry 

function DateFormat(txt, keyCode) {
    // alert(txt);

    var isShift = false;
    var seperator = "-";

    if (keyCode == 16)
        isShift = true;
    //Validate that its Numeric
    if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 ||
         keyCode <= 37 || keyCode <= 39 ||
         (keyCode >= 96 && keyCode <= 105)) && isShift == false) {
        if ((txt.value.length == 2 || txt.value.length == 5) && keyCode != 8) {
            txt.value += seperator;
        }
        return true;
    }
    else {
        return false;
    }
}



function ValidateDate(txt, keyCode) {
    var isShift = false;
    var seperator = "-";

    if (keyCode == 16)
        isShift = false;
    var val = txt.value;

    if (val.length == 10) {
        var splits = val.split("-");
        var date = new Date(splits[1] + "-" + splits[0] + "-" + splits[2]);

        //Validation for Dates
        if (date.getDate() == splits[0] && date.getMonth() + 1 == splits[1]
            && date.getFullYear() == splits[2]) {
            if (RangeValidation(date) != false) {
                if (date.getDate() < 10)
                    date = "0" + date.getDate() + "-" + GetMonthName(date.getMonth() + 1) + "-" + date.getFullYear();
                else
                    date = date.getDate() + "-" + GetMonthName(date.getMonth() + 1) + "-" + date.getFullYear();
                txt.value = date;
            }
        }
        else {
            $(txt).val('');
            DialogWarningMessage("Please Enter Valid Date" + val);
            return false;
        }
        // return date;

        //Range Validation
        if (txt.id.indexOf("txtRange") != -1)
            RangeValidation(dt);

        //BirthDate Validation
        //if (txt.id.indexOf("txtBirthDate") != -1)
        //    BirthDateValidation(dt)
    }

}


function RangeValidation(dt) {
    var startrange = new Date(Date.parse("01/01/1900"));
    var endrange = new Date(Date.parse("12/31/2099"));
    if (dt < startrange || dt > endrange) {
        DialogWarningMessage("Date should be between 01/01/1900 and 31/12/2099");
        return false;
    }
}

// Get the Date by Adding 1 year of Selected Date
function GetDate(dt, ctrl) {
    var arr = dt.split('-');
    var NewDate = arr[2] + "," + arr[1] + "," + arr[0];
    date = new Date(NewDate);
    // var myDate = new Date(newdate);
    date.setFullYear(date.getFullYear() + 1);
    date.setDate(date.getDate() - 1);
    date.setMonth(date.getMonth() + 1);
    // if (date.getMonth()==2 || date.getMonth()==4 || date.getMonth()==6 ||date.getMonth()==9||date.getMonth()==11){
    //  EndDate = date.getDate() + "-"+ GetMonthName(date.getMonth()-1)+ "-" + date.getFullYear();}
    //   else{
    EndDate = date.getDate() + "-" + GetMonthName(date.getMonth()) + "-" + date.getFullYear();
    // }
    $('#' + ctrl).val(EndDate);
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Getting min date from dates array
---------------------------------------------------*/
function min_date(all_dates) {
    if (all_dates.length > 0) {
        var min_dt = all_dates[0],
            min_dtObj = new Date(all_dates[0]);

        all_dates.forEach(function (dt, index) {
            if (new Date(dt) < min_dtObj) {
                min_dt = dt;
                min_dtObj = new Date(dt);
            }
        });

        return min_dt;
    } else
        return null;
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Gettting all cities by passing State ID
---------------------------------------------------*/
function StateChange(id, controlid, cityid) {
    if (id != '' && id != null) {
        $.ajax({
            url: '/Common/GetAllCitiesByStateId',
            type: 'GET',
            data: { stateId: id },
            success: function (result) {
                // CheckSessionVariable(result);
                data = result;
                // alert(data);
                BindDropdown($.parseJSON(data), controlid);
                if (cityid != null)
                    $('#' + controlid).val(cityid);
            },
            error: function () {
                DialogCommomErrorFunction('Error while getting data');
            }
        });
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Gettting all Hospitals by passing City ID
---------------------------------------------------*/
function GetHospitalsByCityChange(id, controlid) {
    if (id != '' && id != null) {
        $.ajax({
            url: '/Common/GetHospitalsByCityId',
            type: 'GET',
            data: { CityId: id },
            success: function (result) {
                // CheckSessionVariable(result);
                data = result;
                // alert(data);

                BindDropdown($.parseJSON(data), controlid);
                MakedropdownasChoosen(controlid, null);
            },
            error: function () {
                DialogCommomErrorFunction('Error while getting data');
            }
        });
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Bind Dropdown by Passing Parent ID 
---------------------------------------------------*/
function BindDropdownByparentID(data, id, parentID) {
    var html = '';//'<option value="">Select</option>';
    $("#" + id + " :gt(0)").remove();
    // alert(parentID);
    if (parentID >= 0) {
        $.each(data, function (i, item) {
            if (item.ParentID == parentID)
                html = html + '<option value="' + item.ID + '">' + item.Name + '</option>';
        });
    }
    // $('#' + id).html(html);
    $('#' + id).append(html);
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Generate Controls Array from one Div or panel
---------------------------------------------------*/
function GenerateControlIdsArray() {

    ids = ["txtExternalValue", "txtExternalPercentage", "txtInternalCappingValue", "txtInternalCappingPer", "txtDuration", "txtClaimLimit", "txtClaimPer", "txtIndividualLimit", "txtIndividualPer", "txtIndividualCounts", "txtFamilyLimit", "txtFamilyPer", "txtFamilyCounts", "txtPolicyLimit", "txtPolicyPer", "txtPolicyCounts", "txtCorporeateLimit", "txtCorporeatePer", "txtCorporateCounts", "txtCopayAmount", "txtCopayPer", "txtAge", "chkICRCopay", "txt1AValue", "txt1APer", "txt1BValue", "txt1BPer", "txt1CValue", "txt1CPer", "txtEffectiveDate", "txtRemarks", "chkCopytoPolicy", "ddlConditionCategory", "ddlConditionName", "ddlCoverage", "ddlCoverageType", "ddlServiceType", "ddlServiceSubType", "ddlRelationGroup", "ddlRelations", "ddlCompareFrom", "ddlExpression", "ddlCompareTo", "ddlDurationType", "ddlLimitCategory", "ddlLessMore", "ddlAgeType", "ddlZone", "ddlNonNW", "ddlClaimType", "ddlState", "ddlCity", "ddlPayer", "ddlNatureofTreatment", "ddlAdmissionType", "ddlAilments", "ddlApplicableTo", "ddlPolicyEndorsement", "ddlProductCoverage", "ddlGrade", "ddlDesignation"];
    spids = ["ID", "BPConditionID", "isCovered", "CoverageType_P49", "ServiceTypeID", "ServiceSubTypeID", "RelGroupID_P26", "RelationshipID", "BPComparisionFrom_P52", "ExpressionID_P17", "BPComparisionTo_P52", "ExternalValuePerc", "ExternalValueAbs", "InternalValuePerc", "InternalValueAbs", "Duration", "DurationType_P18", "CorporateLimit", "CorporatePerc", "CorporateClaimCount", "PolicyLimit", "Policyperc", "PolicyClaimCount", "FamilyLimit", "FamilyPerc", "FamilyClaimCount", "IndividualLimit", "IndividualPerc", "IndividualClaimCount", "ClaimLimit", "ClaimPerc", "TPAProcedureID", "CopayPerc", "CopayValue", "isLess", "Age", "AgeTypeID", "InsZone", "Grade", "Designation", "NetworkType_P50", "ClaimTypeID", "NatureofTreatment_P43", "AdmissionTypeID", "CityID", "isICRCopay", "PayerID", "ProductCoverageID", "APerc", "AValue", "BPerc", "BValue", "CPerc", "CValue", "LimitCatg_P29", "ApplicableTo_P11", "PolicyID", "ENDR_ID", "EffectiveDate", "Remarks", "IsPolicyCopy"];

    iddds = [{ "id": "txtExternalValue", "controltype": "textbox" }, { "id": "txtExternalPercentage", "controltype": "textbox" }, { "id": "txtInternalCappingValue", "controltype": "textbox" }, { "id": "txtInternalCappingPer", "controltype": "textbox" }, { "id": "txtDuration", "controltype": "textbox" }, { "id": "txtClaimLimit", "controltype": "textbox" }, { "id": "txtClaimPer", "controltype": "textbox" }, { "id": "txtIndividualLimit", "controltype": "textbox" }, { "id": "txtIndividualPer", "controltype": "textbox" }, { "id": "txtIndividualCounts", "controltype": "textbox" }, { "id": "txtFamilyLimit", "controltype": "textbox" }, { "id": "txtFamilyPer", "controltype": "textbox" }, { "id": "txtFamilyCounts", "controltype": "textbox" }, { "id": "txtPolicyLimit", "controltype": "textbox" }, { "id": "txtPolicyPer", "controltype": "textbox" }, { "id": "txtPolicyCounts", "controltype": "textbox" }, { "id": "txtCorporeateLimit", "controltype": "textbox" }, { "id": "txtCorporeatePer", "controltype": "textbox" }, { "id": "txtCorporateCounts", "controltype": "textbox" }, { "id": "txtCopayAmount", "controltype": "textbox" }, { "id": "txtCopayPer", "controltype": "textbox" }, { "id": "txtAge", "controltype": "textbox" }, { "id": "chkICRCopay", "controltype": "checkbox" }, { "id": "txt1AValue", "controltype": "textbox" }, { "id": "txt1APer", "controltype": "textbox" }, { "id": "txt1BValue", "controltype": "textbox" }, { "id": "txt1BPer", "controltype": "textbox" }, { "id": "txt1CValue", "controltype": "textbox" }, { "id": "txt1CPer", "controltype": "textbox" }, { "id": "txtEffectiveDate", "controltype": "textbox" }, { "id": "txtRemarks", "controltype": "textbox" }, { "id": "chkCopytoPolicy", "controltype": "checkbox" }, { "id": "ddlConditionCategory", "controltype": "dropdown" }, { "id": "ddlConditionName", "controltype": "dropdown" }, { "id": "ddlCoverage", "controltype": "dropdown" }, { "id": "ddlCoverageType", "controltype": "dropdown" }, { "id": "ddlServiceType", "controltype": "dropdown" }, { "id": "ddlServiceSubType", "controltype": "dropdown" }, { "id": "ddlRelationGroup", "controltype": "dropdown" }, { "id": "ddlRelations", "controltype": "dropdown" }, { "id": "ddlCompareFrom", "controltype": "dropdown" }, { "id": "ddlExpression", "controltype": "dropdown" }, { "id": "ddlCompareTo", "controltype": "dropdown" }, { "id": "ddlDurationType", "controltype": "dropdown" }, { "id": "ddlLimitCategory", "controltype": "dropdown" }, { "id": "ddlLessMore", "controltype": "dropdown" }, { "id": "ddlAgeType", "controltype": "dropdown" }, { "id": "ddlZone", "controltype": "dropdown" }, { "id": "ddlNonNW", "controltype": "dropdown" }, { "id": "ddlClaimType", "controltype": "dropdown" }, { "id": "ddlState", "controltype": "dropdown" }, { "id": "ddlCity", "controltype": "dropdown" }, { "id": "ddlPayer", "controltype": "dropdown" }, { "id": "ddlNatureofTreatment", "controltype": "dropdown" }, { "id": "ddlAdmissionType", "controltype": "dropdown" }, { "id": "ddlAilments", "controltype": "dropdown" }, { "id": "ddlApplicableTo", "controltype": "dropdown" }, { "id": "ddlPolicyEndorsement", "controltype": "dropdown" }, { "id": "ddlProductCoverage", "controltype": "dropdown" }, { "id": "ddlGrade", "controltype": "dropdown" }, { "id": "ddlDesignation", "controltype": "dropdown" }];
    var idList = [{ "id": "hdnRuleID", "controltype": "hidden", "innerid": "ID" }, { "id": "txtExternalValue", "controltype": "textbox", "innerid": "ExternalValueAbs" }, { "id": "txtExternalPercentage", "controltype": "textbox", "innerid": "ExternalValuePerc" }, { "id": "txtInternalCappingValue", "controltype": "textbox", "innerid": "InternalValueAbs" }, { "id": "txtInternalCappingPer", "controltype": "textbox", "innerid": "InternalValuePerc" }, { "id": "txtDuration", "controltype": "textbox", "innerid": "Duration" }, { "id": "txtClaimLimit", "controltype": "textbox", "innerid": "ClaimLimit" }, { "id": "txtClaimPer", "controltype": "textbox", "innerid": "ClaimPerc" }, { "id": "txtIndividualLimit", "controltype": "textbox", "innerid": "IndividualLimit" }, { "id": "txtIndividualPer", "controltype": "textbox", "innerid": "IndividualPerc" }, { "id": "txtIndividualCounts", "controltype": "textbox", "innerid": "IndividualClaimCount" }, { "id": "txtFamilyLimit", "controltype": "textbox", "innerid": "FamilyLimit" }, { "id": "txtFamilyPer", "controltype": "textbox", "innerid": "FamilyPerc" }, { "id": "txtFamilyCounts", "controltype": "textbox", "innerid": "FamilyClaimCount" }, { "id": "txtPolicyLimit", "controltype": "textbox", "innerid": "PolicyLimit" }, { "id": "txtPolicyPer", "controltype": "textbox", "innerid": "Policyperc" }, { "id": "txtPolicyCounts", "controltype": "textbox", "innerid": "PolicyClaimCount" }, { "id": "txtCorporeateLimit", "controltype": "textbox", "innerid": "CorporateLimit" }, { "id": "txtCorporeatePer", "controltype": "textbox", "innerid": "CorporatePerc" }, { "id": "txtCorporateCounts", "controltype": "textbox", "innerid": "CorporateClaimCount" }, { "id": "txtCopayAmount", "controltype": "textbox", "innerid": "CopayValue" }, { "id": "txtCopayPer", "controltype": "textbox", "innerid": "CopayPerc" }, { "id": "txtAge", "controltype": "textbox", "innerid": "Age" }, { "id": "chkICRCopay", "controltype": "checkbox", "innerid": "isICRCopay" }, { "id": "txt1AValue", "controltype": "textbox", "innerid": "AValue" }, { "id": "txt1APer", "controltype": "textbox", "innerid": "APerc" }, { "id": "txt1BValue", "controltype": "textbox", "innerid": "BValue" }, { "id": "txt1BPer", "controltype": "textbox", "innerid": "BPerc" }, { "id": "txt1CValue", "controltype": "textbox", "innerid": "CValue" }, { "id": "txt1CPer", "controltype": "textbox", "innerid": "CPerc" }, { "id": "txtEffectiveDate", "controltype": "textbox", "innerid": "EffectiveDate" }, { "id": "txtRemarks", "controltype": "textbox", "innerid": "Remarks" }, { "id": "chkCopytoPolicy", "controltype": "checkbox", "innerid": "IsPolicyCopy" }, { "id": "ddlConditionCategory", "controltype": "dropdown", "innerid": "BPConditionID" }, { "id": "ddlConditionName", "controltype": "dropdown", "innerid": "BPConditionID" }, { "id": "ddlCoverage", "controltype": "dropdown", "innerid": "isCovered" }, { "id": "ddlCoverageType", "controltype": "dropdown", "innerid": "CoverageType_P49" }, { "id": "ddlServiceType", "controltype": "dropdown", "innerid": "ServiceTypeID" }, { "id": "ddlServiceSubType", "controltype": "dropdown", "innerid": "ServiceSubTypeID" }, { "id": "ddlRelationGroup", "controltype": "dropdown", "innerid": "RelGroupID_P26" }, { "id": "ddlRelations", "controltype": "dropdown", "innerid": "RelationshipID" }, { "id": "ddlCompareFrom", "controltype": "dropdown", "innerid": "BPComparisionFrom_P52" }, { "id": "ddlExpression", "controltype": "dropdown", "innerid": "ExpressionID_P17" }, { "id": "ddlCompareTo", "controltype": "dropdown", "innerid": "BPComparisionTo_P52" }, { "id": "ddlDurationType", "controltype": "dropdown", "innerid": "DurationType_P18" }, { "id": "ddlLimitCategory", "controltype": "dropdown", "innerid": "LimitCatg_P29" }, { "id": "ddlLessMore", "controltype": "dropdown", "innerid": "isLess" }, { "id": "ddlAgeType", "controltype": "dropdown", "innerid": "AgeTypeID" }, { "id": "ddlZone", "controltype": "dropdown", "innerid": "InsZone" }, { "id": "ddlNonNW", "controltype": "dropdown", "innerid": "NetworkType_P50" }, { "id": "ddlClaimType", "controltype": "dropdown", "innerid": "ClaimTypeID" }, { "id": "ddlCity", "controltype": "dropdown", "innerid": "CityID" }, { "id": "ddlPayer", "controltype": "dropdown", "innerid": "PayerID" }, { "id": "ddlNatureofTreatment", "controltype": "dropdown", "innerid": "NatureofTreatment_P43" }, { "id": "ddlAdmissionType", "controltype": "dropdown", "innerid": "AdmissionTypeID" }, { "id": "ddlAilments", "controltype": "dropdown", "innerid": "TPAProcedureID" }, { "id": "ddlApplicableTo", "controltype": "dropdown", "innerid": "ApplicableTo_P11" }, { "id": "ddlPolicyEndorsement", "controltype": "dropdown", "innerid": "PolicyID" }, { "id": "ddlPolicyEndorsement", "controltype": "dropdown", "innerid": "ENDR_ID" }, { "id": "ddlProductCoverage", "controltype": "dropdown", "innerid": "ProductCoverageID" }, { "id": "ddlGrade", "controltype": "dropdown", "innerid": "Grade" }, { "id": "ddlDesignation", "controltype": "dropdown", "innerid": "Designation" }];
    var idsArray = [];


    $.each(idList, function (i, item) {
        item["sno"] = i;
        newidlist.push(item);
    });
    // $("select").each(function () { ddlIDs.push(this.id); });
    //$.each(ids, function (i, item) {
    //    var controlobj = {};
    //    if (item.indexOf('txt') == 0) {
    //        //  var controlobj = {};
    //        controlobj["id"] = item;
    //        controlobj["controltype"] = "textbox";
    //        controlobj["innerid"] = "";
    //    }
    //    else if (item.indexOf('ddl') == 0) {
    //        //  var controlobj = {};
    //        controlobj["id"] = item;
    //        controlobj["controltype"] = "dropdown";
    //        controlobj["innerid"] = "";
    //    }
    //    else if (item.indexOf('chk') == 0) {

    //        controlobj["id"] = item;
    //        controlobj["controltype"] = "checkbox";
    //        controlobj["innerid"] = "";
    //    }
    //    idsArray.push(controlobj);
    //})


    //  Changing Slno 
    var spids = ["ID", "BPConditionID", "isCovered", "CoverageType_P49", "ServiceTypeID", "ServiceSubTypeID", "RelGroupID_P26", "RelationshipID", "BPComparisionFrom_P52", "ExpressionID_P17", "BPComparisionTo_P52", "ExternalValuePerc", "ExternalValueAbs", "InternalValuePerc", "InternalValueAbs", "Duration", "DurationType_P18", "CorporateLimit", "CorporatePerc", "CorporateClaimCount", "PolicyLimit", "Policyperc", "PolicyClaimCount", "FamilyLimit", "FamilyPerc", "FamilyClaimCount", "IndividualLimit", "IndividualPerc", "IndividualClaimCount", "ClaimLimit", "ClaimPerc", "TPAProcedureID", "CopayPerc", "CopayValue", "isLess", "Age", "AgeTypeID", "InsZone", "Grade", "Designation", "NetworkType_P50", "ClaimTypeID", "NatureofTreatment_P43", "AdmissionTypeID", "CityID", "isICRCopay", "PayerID", "ProductCoverageID", "APerc", "AValue", "BPerc", "BValue", "CPerc", "CValue", "LimitCatg_P29", "ApplicableTo_P11", "PolicyID", "ENDR_ID", "EffectiveDate", "Remarks", "IsPolicyCopy"];
    var done = true;
    $.each(spids, function (i, item) {
        $.each(idsArray, function (a, b) {

            if (done) {
                if (item == "BPConditionID" && b.id == "ddlConditionName") {
                    done = false;
                    var obj = { "id": "ddlConditionName", "controltype": "dropdown", "innerid": "BPConditionID", "sno": i + 1 };
                }
            }
            if (done) {
                if (item == b.innerid) {
                    b.sno = i;
                    idsnewArray.push(b);
                }
            }
            else {
                if (item == b.innerid) {
                    b.sno = i + 1;
                    idsnewArray.push(b);
                }
            }
        })
        // idsnewArray.push(item);
    });
    console.log(idsnewArray);
}


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Convert normal dropdown as Choosen Select
---------------------------------------------------*/
function MakedropdownasChoosen(id, e) {

    //$('#' + id).addClass('chosen-select');
    //$(".chosen-select").chosen({ disable_search_threshold: 10 });//.change(function (event) { if (event.target == this) { } });
    //$(e).remove();
    if ($('#ddlmuHospital_chosen').length > 0) {
        // $('#' + id).removeClass('chosen-select').show();
        $('#' + id).removeClass('chosen-select').removeAttr('style');
        $('#' + id + '_chosen').remove();
    } else {
        $('#' + id).addClass('chosen-select');
        $(".chosen-select").chosen({ disable_search_threshold: 10 });//.change(function (event) { if (event.target == this) { } });
        // $(e).remove();
    }
    // $('#ddlmuHospital').addClass('chosen-select')
}

function EmailValidate(id) {
    //  $("#" + id).change(function () {
    //var a = $("#" + id).val();
    //var RegExEmail = /^[_a-z0-9-A-Z-]+(\.[_a-z0-9-A-Z-]+)*@@[a-z0-9-A-Z-]+(\.[a-z0-9-A-Z-]+)*(\.[a-z]{2,4})$/;
    //if (!RegExEmail.test(a)) {
    //    $("#" + id).focus();
    //    DialogWarningMessage("Please Enter Valid E-mail Adress");
    //   // $("#" + id).val("");
    //    return false;
    //}
    //});

    var RegExEmail = /^[_a-z0-9-A-Z-]+(\.[_a-z0-9-A-Z-]+)*@[a-z0-9-A-Z-]+(\.[a-z0-9-A-Z-]+)*(\.[a-z]{2,4})$/;
    var a = $("#" + id).val();
    if (!RegExEmail.test(a)) {

        DialogWarningMessage("Please Enter Valid E-mail Address");
        //$("#"+id).val("");
        $("#" + id).focus();
        return false;
    } else {
        return true;
    }
}
//-------------------------------Bug Description Insert  start Here

/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Bug Desciption Dialog open
---------------------------------------------------*/
function BugDescriptionDialog() {
    //e.preventDefault();
    //$('#dialogbugInnerText').text(innertext);
    $("#dialog-bugtracker").removeClass('hide').dialog({
        resizable: false,
        width: '750',
        height: 400,
        modal: true,
        title: "Report a bug",
        //title:"<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='ace-icon fa fa-save-o bigger-110'></i>&nbsp; Proceed",
                "class": "btn btn-danger btn-minier",
                click: function () {
                    Reportbug();
                    $('#txtbugDesc').val('');
                    $('#txtloginuser').val('');
                    $(this).dialog("close");
                    //  if (successmethod != null) successmethod();
                    return true;

                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Cancel",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        ]
    });
}

/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Insert Bug or observation Description 
---------------------------------------------------*/
function Reportbug() {
    try {
        var desc = $('#txtbugDesc').val();
        var user = $('#txtloginuser').val();
        var pagename = "";
        if (desc != '' && user != '') {
            $.ajax({
                url: '/Common/Bug_Insert',
                type: 'POST',
                data: { Description: desc, Username: user, pagename: pagename },
                success: function (result) {
                    CheckSessionVariable(result);
                    // alert(result);
                    DialogResultMessage("Your observation is recorded successfully...");
                },
                error: function () {

                    DialogCommomErrorFunction('Error while Processing')
                }
            });
        } else {
            DialogWarningMessage('Username and description fields are mandatory')
        }
    } catch (e) {
        DialogCommomErrorFunction('Error while Processing')
    }
}

//-------------------------------Bug Description Insert  END


/*-------------------------------------------------
Code Written By B. Srinu  on 01-Dec-2015
Description : Calculating Days Count between two dates 
---------------------------------------------------*/
//daydiff($('#dtDOA').val(), $('#dtDOD').val())
function daydiff(first, second) {
    return (Math.round((new Date(second) - new Date(first)) / (1000 * 60 * 60 * 24)) + 1);
}


function welcomeMsg(rolename) {
    var _html = '<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert"><img src="/Content/images/cross.png" alt="Cross" /></button><i class="ace-icon fa fa-check green"></i>Welcome <strong class="green">    ' + rolename + '<small> User</small></strong></div>';
    $('#divwelcomeMsg').html(_html);
}


function ValidateMobileNumber(ctrl) {
    var value = $('#' + ctrl).val();
    var firstChar = value.substring(0, 1);
    if (value.length < 10) {
        $('#' + ctrl).val('');
        alert('Invalid Mobile Number');
    }
    else if (parseInt(firstChar) != 7 && parseInt(firstChar) != 8 && parseInt(firstChar) != 9) {
        $('#' + ctrl).val('');
        alert('Invalid Mobile Number');
    }
}

//Get LessMore Prop with Id
function GetLessMoreNamePropwithId(id) {

    var name = '';
    if (id == 1 || id == true) {
        name = "Less";
    } else if (id == 0 || id == false) {
        name = "More";
    }
    return name;
}

function MemberDetails_View(_memberPolicyID) {
    if (_memberPolicyID != null) {
        //  if ($('#tblAuditTrail tbody').children().length == 0) {
        $.ajax({
            type: "GET",
            url: "/Common/MemberDetails_Retrieve",
            contentType: 'application/json;charset=utf-8',
            //processData: false,
            data: { memberPolicyID: _memberPolicyID },
            success: function (data) {
                CheckSessionVariable(data);
                data = $.parseJSON(data);

                if (data == null || data == "") {
                    //alert('Data not found.');
                }
                else Bind_MemberData(data);
            },
            error: function (e, x) {
                ShowResultMessage('ErrorMessage', e.responseText);
            }
        });
    }
    // }
}
function Bind_BenifitPlanData() {
    debugger;
    alert(1);
    var PolicyId = $("#lblPolacyID").text();
    $.ajax({
        url: '/Common/Policy_Data_Retrieve',
        type: 'POST',
        dataType: "json",
        data: { PolicyId: PolicyId },
        success: function (data1) {

            if (data1 != null)
                CommonPopup('/BenefitPlan/BPCreation/?Id=' + data1.Table[0].RequestID + '&BPF=1&BPVF=1')
            //window.open('/BenefitPlan/BPCreation/?Id=' + data1.Table[0].RequestID + '&BPF=1&BPVF=1', '');
        }
    });


};
function Bind_MemberData(data) {
    $('#tblSumInsured_View tbody,#tblFamilyMember_View tbody').html('');
    if (data.Table.length > 0) {
        $("#spnInsurerName_View").text(data.Table[0].Insurance);
        $("#spnPayerName_View").text(data.Table[0].Payername);
        $("#spnCorpName_View").text(MakeNEUasNotApplicable(data.Table[0].Corporate));
        $("#spnProductName_View").text(data.Table[0].PRoduct);


        //------------------------------vijitha 
        //$("#spnBPName_View").html(data.Table[0].BPName);


        // $("#lblCurrentBenefitPlan").text(data.Table[0].BPName);

        $("#spnBPName_View").text(data.Table[0].BPName);

        var PolicyId = data.Table[0].PolicyID;

        $("#lblPolacyID").text(PolicyId)

        $("#spnPolicyStatus_View").text(data.Table[0].PolicyStatus);

        if (data.Table[0].PolicyStatus == "Active") {
            $("#spnPolicyStatus_View").addClass("label label-success arrowed-in arrowed-in-right");
        }
        else if (data.Table[0].PolicyStatus == "InActive") {
            $("#spnPolicyStatus_View").addClass("label label-warning arrowed-in arrowed-in-right");
        }
        else if (data.Table[0].PolicyStatus == "Blacklist") {
            $("#spnPolicyStatus_View").addClass("label label-inverse  arrowed-in arrowed-in-right");
        }
        else if (data.Table[0].PolicyStatus == "Entry") {
            $("#spnPolicyStatus_View").addClass("label label-info arrowed-in arrowed-in-right");
        }

        $("#spnPolicyType_View").text(data.Table[0].PolicyType);
        $("#spnPolicyNumber_View").text(data.Table[0].PolicyNo);

        chkDatewithCntrl_ToText(data.Table[0].PolicyStartdate, 'spnPolicyStartDate_View');
        chkDatewithCntrl_ToText(data.Table[0].PolicyEnddate, 'spnPolicyEndDate_View');
        chkDatewithCntrl_ToText(data.Table[0].PolicyInceptionDate, 'spnPolicyInceptionDate_View');
        $("#spnEmpID_View").text(data.Table[0].EmployeeID);
        $("#spnBrokerAgentName_View").text(MakeEmptyfromUndefinedorNull(data.Table[0].BrokerName));//*************************
        $("#spnUHID_View").text(data.Table[0].uhidno);
        // $("#spnTrackBlock_View").text(data.Table[0].isSuspicious);

        //if (data.Table[0].isSuspicious == "0") {
        //    $("#spnisSuspicious_View").addClass("label label-inverse  arrowed-in arrowed-in-right");
        //    $("#spnisSuspicious_View").text('Block');
        //}
        if (data.Table[0].isSuspicious == "0" || data.Table[0].isSuspicious == null || data.Table[0].isSuspicious == undefined) {
            $("#spnisSuspicious_View").addClass("label label-success  arrowed-in arrowed-in-right");
            $("#spnisSuspicious_View").text('Clean');
        }
        else if (data.Table[0].isSuspicious == "1") {
            $("#spnisSuspicious_View").addClass("label label-danger arrowed-in arrowed-in-right");
            $("#spnisSuspicious_View").text('Suspicious');
        }
        else if (data.Table[0].PolicyStatus == null) {
            $("#spnisSuspicious_View").addClass("label label-success  arrowed-in arrowed-in-right");
            $("#spnisSuspicious_View").text('');
            //$("#spnisSuspicious_View").text('Clean');
        }
        if (data.Table[0].isVIP == 0 || data.Table[0].isVIP == true) {
            $("#divVIP_View").show();
            $("#spnVIPStatus_View").addClass('label label-info arrowed');
            $("#spnVIPStatus_View").text('VIP');
        }
        $("#spnMemberName_View").text(data.Table[0].Salutation + '.' + data.Table[0].MemberName + '(' + data.Table[0].Gender + ')');
        $("#spnMemberRelationship_View").text(data.Table[0].Relationship);
        //chkDatewithCntrl(data.Table[0].DOB, 'spnMemberDOB_View');
        $("#spnMemberDOB_View").text((chkDate(data.Table[0].DOB)) + '(' + data.Table[0].Age + ':' + data.Table[0].Agetype + ')');
        $("#spnMemberDOJ_View").text(chkDate(data.Table[0].DOJ));
        $("#spnMemberMaritulStatus_View").text(data.Table[0].MaritalStatus);
        $("#spnMemberOccupation_View").text(data.Table[0].Occupation);


        if (data.Table[0].Status == "Active") {
            if (data.Table[0].isSuspicious == "1") {
                $("#spnMemberStatus_View").addClass("label label-danger arrowed-in arrowed-in-right");
                $("#spnMemberStatus_View").text('Active');
            }
            else {
                $("#spnMemberStatus_View").addClass("label label-success arrowed-in arrowed-in-right");
                $("#spnMemberStatus_View").text('Active');
            }
        }
        else if (data.Table[0].Status == "InActive") {
            $("#spnMemberStatus_View").addClass("label label-warning arrowed-in arrowed-in-right");
            $("#spnMemberStatus_View").text('InActive');
        }
        $("#spnMemberCommencementDate_View").text(JSONDate2(data.Table[0].MemberCommencingDate));

        $("#spnMemberEndDate_View").text(JSONDate2(data.Table[0].MemberEndDate));


        $("#spnMemberDesignation_View").val(data.Table[0].Designation);
        $("#spnMemberGrade_View").text(data.Table[0].Grade);
        $("#spnMemberUnit_View").text(data.Table[0].Unit);
        $("#spnMemberDivision_View").text(data.Table[0].Division);


        $("#spnPassportNo_View").text(data.Table[0].PassportNo);
        $("#spnPanNo_View").text(data.Table[0].PanNo);
        $("#spnDrivingLicence_View").text(data.Table[0].DrivingLiscenceNo);
        $("#spnAadharID_View").text(data.Table[0].AadharID);
        $("#spnUniqueID_View").text(data.Table[0].UniqueID);
        $("#spnRationCardID_View").text(data.Table[0].RationCardNo);
        $("#spnVoterID_View").text(data.Table[0].VoterID);
        $("#spnBloodGroup_View").text(data.Table[0].BloodGroup);

        $("#spnInsUHIDNo_View").text(data.Table[0].InsUhidno);
        $("#spnInsEmployeeID_View").text(data.Table[0].Ins_EmployeeID);
        $("#spnInsPersonID_View").text(data.Table[0].Ins_Personid);
        $("#spnInsSerialNo_View").text(data.Table[0].Ins_SerialNo);
        $("#spnInsFamilyNo_View").text(data.Table[0].Ins_Familyno);
        $("#spnInsPartyCode_View").text(data.Table[0].Partycode);
        $("#spnRiskID_View").text(data.Table[0].RiskID);

        $("#spnMemberNotes_View").text(data.Table[0].Notes);
        $("#spnMemberRemarks_View").text(data.Table[0].Remarks);
        $("#spnNomineeName_View").text(data.Table[0].NomineeName);
    }
    if (data.Table2.length > 0) {
        for (var i = 0; i < data.Table2.length; i++) {
            var tblBody = '<tr> <td class="numeric" style="text-align: center;">' + data.Table2[i].SICategory + '</td>'
                                + '<td  class="numeric" style="text-align: center;">' + data.Table2[i].SIType + '</td>'
                        + '<td  class="numeric" style="text-align: center;">' + data.Table2[i].SumInsured + '</td>'
                         + '<td  class="numeric" style="text-align: center;">' + data.Table2[i].CB_Amount + '</td>'
                        + '<td  style="text-align: center;"class="numeric"><input type="button" class="btn btn-primary" id="btn_' + data.Table2[i].SICategory + '" value="View BalanceSI" onclick="GetBalanceSI(' + data.Table2[i].memberpolicyid + ',' + data.Table2[i].SITypeID + ',' + data.Table2[i].SICategoryID_P20 + ')"> <input readonly="readonly" type="text" class="form-group" id="txtBalanceSIView_' + data.Table2[i].SICategoryID_P20 + '"></td>'
                        + '<td style="text-align: center;" class="numeric">' + chkDate(data.Table2[i].EffectiveFrom) + '</td></tr>';
            $('#tblSumInsured_View tbody').append(tblBody);
        }
    }
    if (data.Table4.length > 0) {
        for (var i = 0; i < data.Table4.length; i++) {
            var tblBody = '<tr> <td class="numeric" style="text-align: center;"><a><label class="btn-minier" style="color:red;" onclick="MemberDetails_View(' + data.Table4[i].ID + ')">' + data.Table4[i].uhidno + '</a></td>'
                        + '<td  class="numeric" style="text-align: center;">' + data.Table4[i].MemberName + '</td>'
                        + '<td  style="text-align: center;"class="numeric">' + data.Table4[i].Gender + '</td>'
                         + '<td  style="text-align: center;"class="numeric">' + chkDate(data.Table4[i].DOB) + '</td>'
                        + '<td style="text-align: center;" class="numeric">' + data.Table4[i].Age + '(' + data.Table4[i].Agetype + ')</td></tr>';
            $('#tblFamilyMember_View tbody').append(tblBody);
        }
    }

    if (data.Table1.length > 0) {
        $("#spnAddress1_View").text(data.Table1[0].Address1);
        $("#spnAddress2_View").text(data.Table1[0].Address2);
        $("#spnLocation_View").text(data.Table1[0].Location);
        $("#spnStateDist_View").text(data.Table1[0].District + '(' + data.Table1[0].State + ')');
        $("#spnCity_View").text(data.Table1[0].City);
        $("#spnMobileNo_View").text(data.Table1[0].MobileNo);
        $("#spnEmail_View").text(data.Table1[0].EmailID);

        $("#spnMemberMobileNO_View").text(data.Table1[0].MobileNo);
        $("#spnMemberEmail_View").text(data.Table1[0].EmailID);
    }
    if (data.Table3.length > 0) {
        $("#spnIFSCCode_View").text(data.Table3[0].IFSCCode);
        $("#spnBankName_View").text(data.Table3[0].Bank);
        $("#spnBranchName_View").text(data.Table3[0].Branch);
        $("#spnAccountNo_View").text(data.Table3[0].AccountNo);
        //$("#spnAccountName_View").text(data.Table2[0].UniqueID);
        //$("#spnAccountType_View").text(data.Table2[0].RationCardNo);
    }
    if (data.Table.length > 0)
        $('#divMemberImage').html('<img id="memberImage_View" style="width: 120px; height: 150px;" src="/Common/GetMemberImage?MemberPolicyID=' + data.Table[0].MainmemberID + ' "/>');

}
function Bind_BPMemberData(data) {
    $('#tblFamilyDefinition_View tbody,#tblSumInsured_View tbody').html('');
    if (data.Table.length > 0) {
        $("#spnBPName_View").text(data.Table[0].NAME);
        $("#spnInsurerName_View").text(data.Table[0].ISSUEID);
        $("#spnPayerName_View").text(MakeNEUasNotApplicable(data.Table[0].PAYERID));
        $("#spnPolicyType_View").text(data.Table[0].POLICYTYPEID);
        $("#spnProductName_View").text(data.Table[0].PRODUCTID);
        $("#spnSIType_View").text(data.Table[0].SITYPEID)
        $("#spnCorpName_View").text(MakeNEUasNotApplicable(data.Table[0].CORPORATEID));
        $("#spnPolicyNumber_View").text(MakeNEUasNotApplicable(data.Table[0].POLICYID));
        $("#spnFamilySizewithSelf_View").text(data.Table[0].FAMILYSIZE);
        $("#spnBPStatus_View").text(data.Table[0].StatusID);
        $("#spnCreatedBy_View").text(data.Table[0].CreatedUserRegionID);
        $("#spnCreateddate_View").text(JSONDate2(data.Table[0].CreatedDate))

        //$("#spnPolicyStatus_View").text(data.Table[0].PolicyStatus);

        //if (data.Table[0].PolicyStatus == "Active") {
        //    $("#spnPolicyStatus_View").addClass("label label-success arrowed-in arrowed-in-right");
        //}
        //else if (data.Table[0].PolicyStatus == "InActive") {
        //    $("#spnPolicyStatus_View").addClass("label label-warning arrowed-in arrowed-in-right");
        //}
        //else if (data.Table[0].PolicyStatus == "Blacklist") {
        //    $("#spnPolicyStatus_View").addClass("label label-inverse  arrowed-in arrowed-in-right");
        //}
        //else if (data.Table[0].PolicyStatus == "Entry") {
        //    $("#spnPolicyStatus_View").addClass("label label-info arrowed-in arrowed-in-right");
        //}


    }
    if (data.Table1.length > 0) {
        BindGridResults(data.Table1, 'tblFamilyDefinition_View', 10);
    }
    if (data.Table2.length > 0) {
        //BindGridResults(data.Table2, 'tblSumInsured_View', 10);
        BindSumInsuredGrid(data.Table2);
    }

}

function BindSumInsuredGrid(data) {
    $('#tblSumInsured_View tbody').html('');
    if (data.length > 0) {
        $.each(data, function (i, item) {
            var _newtr = '<td>' + item.SUMINSURED + ' [ ' + item.ID + ' ]  </label></td>'
      + '<td> ' + item.SICATEGORY + '</td>'
      + '<td class="age"> ' + item.SITYPEID + ' </td>'
     // + '<td class="age"> <a onclick="DesignFamilyDefinitionEditGrid(' + item.ID + ')">Family Definition</a>  </td>'
      + '<td class="age"> ' + item.APPLICABLETO_P11 + '  </td>'
       + '<td>' + ((item.APPLICABLETO_11 == 33) ? item.POLICYID : item.ENDR_ID) + '</td>'
      + '<td>' + JSONDate2(item.EFFECTIVEDATE) + '</td>';
            _newtr = _newtr + '<td><a onclick="myPopup(\'/BenefitPlan/BPSIView?SIID=' + item.ID + '\')">Rules View </a></td>';
            $('<tr/>').html(_newtr).appendTo('#tblSumInsured_View tbody');
        });
    }
    else {
        $('#tblSumInsured_View tbody').html('No Data found');
    }
}

function myPopup(url) {

    window.open(url, "", "width=1000, height=600");
}
function GetBalanceSI(memberPolicyID, SIType, SICategoryID) {
    // if ($('#tblSumInsured_View tbody').children().length == 0) {
    $.ajax({
        //type: "GET",
        url: "/Common/BalanceSumInsured_Retrieve",
        contentType: 'application/json;charset=utf-8',
        data: { MemberPolicyID: memberPolicyID, SITypeID: SIType },
        success: function (SIData) {
            CheckSessionVariable(SIData);
            var data = $.parseJSON(SIData);
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].SICategoryID == SICategoryID) {
                        var BalanceSumInsured = (data[i].SumInsured + data[i].CB_Amount) - ((data[i].BlockedAmt) + (data[i].UtilizedAmt));
                        $('#txtBalanceSIView_' + SICategoryID).val(BalanceSumInsured);
                    }
                }
            }
        },
        error: function (e, x) {
            ShowResultMessage('ErrorMessage', e.responseText);
        }
    });
    // }
}

//Date format from 01-Jan-1900 to '' and bind to text
function chkDatewithCntrl_ToText(value, ctrl) {
    if (value != null || value != '') {
        var _val;
        if (JSONDate2(value) == "01-Jan-1900") {
            _val = '';
            $('#' + ctrl).val(_val);
        }
        else {
            _val = JSONDate2(value)
            $('#' + ctrl).text(_val);
        }
    }
}
function chkDatewithCntrl_ToVal(value, ctrl) {
    if (value != null || value != '') {
        var _val;
        if (JSONDate2(value) == "01-Jan-1900") {
            _val = '';
            $('#' + ctrl).val(_val);
        }
        else {
            _val = JSONDate2(value)
            $('#' + ctrl).val(_val);
        }
    }
}

function chkDate(value) {
    if (value != null || value != '') {
        var _val;
        if ((JSONDate2(value) == "01-Jan-1900") || (JSONDate2(value) == "01-Jan-1970")) {
            _val = '';
            return _val;
        }
        else {
            _val = JSONDate2(value)
            return _val;
        }
    }
}
function chkDate_Control(value, ctrl) {
    if (value != null || value != '') {
        var _val;
        if ((JSONDate2(value) == "01-Jan-1900") || (JSONDate2(value) == "01-Jan-1970")) {
            _val = '';
            return _val;
        }
        else {
            _val = JSONDate2(value)
            $('#' + ctrl).val(_val);
        }
    }
}

function CommonPopup(url) {
    var windowObjectReference;
    var strWindowFeatures = "target=_new,menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";

    function openRequestedPopup() {
        windowObjectReference = window.open(url, url, strWindowFeatures);
    }
    openRequestedPopup();
    //window.open(url, "", "width=1000, height=600");
}

function d1(x) {
    switch (x) {
        case '0': n = ""; break;
        case '1': n = " One "; break;
        case '2': n = " Two "; break;
        case '3': n = " Three "; break;
        case '4': n = " Four "; break;
        case '5': n = " Five "; break;
        case '6': n = " Six "; break;
        case '7': n = " Seven "; break;
        case '8': n = " Eight "; break;
        case '9': n = " Nine "; break;
        default: n = "Not a Number";
    }
    return n;
}

function d2(x) {
    switch (x) {
        case '0': n = ""; break;
        case '1': n = ""; break;
        case '2': n = " Twenty "; break;
        case '3': n = " Thirty "; break;
        case '4': n = " Forty "; break;
        case '5': n = " Fifty "; break;
        case '6': n = " Sixty "; break;
        case '7': n = " Seventy "; break;
        case '8': n = " Eighty "; break;
        case '9': n = " Ninety "; break;
        default: n = "Not a Number";
    }
    return n;
}

function d3(x) {
    switch (x) {
        case '0': n = " Ten "; break;
        case '1': n = " Eleven "; break;
        case '2': n = " Twelve "; break;
        case '3': n = " Thirteen "; break;
        case '4': n = " Fourteen "; break;
        case '5': n = " Fifteen "; break;
        case '6': n = " Sixteen "; break;
        case '7': n = " Seventeen "; break;
        case '8': n = " Eighteen "; break;
        case '9': n = " Nineteen "; break;
        default: n = "Not a Number";
    }
    return n;
}

function convert(input, ControlName) {
    if (input != null) {
        var junkVal = input;//document.getElementById('rupees').value;  
        junkVal = Math.floor(junkVal);
        var obStr = new String(junkVal);
        numReversed = obStr.split("");
        actnumber = numReversed.reverse();
        if (Number(junkVal) >= 0) {
            //do nothing  
        }
        else {
            alert('Invalid number');
            return false;
        }
        if (Number(junkVal) == 0) {
            //document.getElementById('container').innerHTML = obStr + '' + 'Rupees Zero Only';
            if (ControlName != null && ControlName != undefined && ControlName != '')
                document.getElementById(ControlName).value = 'Rupees Zero Only';
            else
                return ' Rupees Zero Only';
            return false;
        }
        if (actnumber.length > 9) {
            alert('Number is too big');
            return false;
        }
        var iWords = ["Zero", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine"];
        var ePlace = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
        var tensPlace = ['dummy', ' Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];
        var iWordsLength = numReversed.length;
        var totalWords = "";
        var inWords = new Array();
        var finalWord = "";
        j = 0;
        for (i = 0; i < iWordsLength; i++) {
            switch (i) {
                case 0:
                    if (actnumber[i] == 0 || actnumber[i + 1] == 1) {
                        inWords[j] = '';
                    }
                    else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    inWords[j] = inWords[j] + ' Only';
                    break;
                case 1:
                    tens_complication();
                    break;
                case 2:
                    if (actnumber[i] == 0) {
                        inWords[j] = '';
                    }
                    else if (actnumber[i - 1] != 0 && actnumber[i - 2] != 0) {
                        inWords[j] = iWords[actnumber[i]] + ' Hundred and';
                    }
                    else {
                        inWords[j] = iWords[actnumber[i]] + ' Hundred';
                    }
                    break;
                case 3:
                    if (actnumber[i] == 0 || actnumber[i + 1] == 1) {
                        inWords[j] = '';
                    }
                    else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    if (actnumber[i + 1] != 0 || actnumber[i] > 0) {
                        inWords[j] = inWords[j] + " Thousand";
                    }
                    break;
                case 4:
                    tens_complication();
                    break;
                case 5:
                    if (actnumber[i] == 0 || actnumber[i + 1] == 1) {
                        inWords[j] = '';
                    }
                    else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    inWords[j] = inWords[j] + " Lakh";
                    break;
                case 6:
                    tens_complication();
                    break;
                case 7:
                    if (actnumber[i] == 0 || actnumber[i + 1] == 1) {
                        inWords[j] = '';
                    }
                    else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    inWords[j] = inWords[j] + " Crore";
                    break;
                case 8:
                    tens_complication();
                    break;
                default:
                    break;
            }
            j++;
        }

        function tens_complication() {
            if (actnumber[i] == 0) {
                inWords[j] = '';
            }
            else if (actnumber[i] == 1) {
                inWords[j] = ePlace[actnumber[i - 1]];
            }
            else {
                inWords[j] = tensPlace[actnumber[i]];
            }
        }
        inWords.reverse();
        for (i = 0; i < inWords.length; i++) {
            finalWord += inWords[i];
        }
        if (ControlName != null && ControlName != undefined && ControlName != '')
            document.getElementById(ControlName).value = "Rs. " + finalWord;
        else
            return "Rs. " + finalWord;
    }
}

function lock() {
    if (event.keyCode > 0) {
        event.returnValue = false;
    }
}

/*-------------------------------------------------
Code Written By B. Srinu  on 
---------------------------------------------------*/
//Key Press Allow digits
function Percentagevalidation(e) {
    // alert($(e).val().indexOf('..'));
    if ($(e).val().indexOf('..') != -1) {
        DialogWarningMessage('Please Enter Valid percentage. percentage should between 0 and 100');
        $(e).val('');
        return false;
    } else {
        var x = parseFloat($(e).val()).toFixed(2);
        if (isNaN(x) || x < 0 || x > 100) {
            DialogWarningMessage('Please Enter Valid percentage. percentage should between 0 and 100');
            $(e).val('');
            return false;

        }
    }
}

/* Added by Nagaraju 08 Jan 2016*/
function Add_DateTimePicker(ctrl) {
    $('#' + ctrl).datepicker({
        //changeMonth: true,
        //changeYear: true,
        //minDate: -30,
        //maxDate: "+0M +7D",
        //dateFormat: 'dd-M-yy'

        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-M-yy',
        maxDate: new Date()

    });
}

function ClearControls_SelectOne(divId) {
    $("#" + divId + " INPUT[type='button']").each(function () {
        //  if (this.id == corpMaindiv) { alert(); }
        $(this).removeAttr("disabled");
    });

    var obj = {};
    //Find all input elements
    var panel = $("#" + divId);
    var inpts = panel.find("input,textarea,select");
    // var txtArea = panel.find("textarea");

    if (inpts.length > 0) {
        $.each(inpts, function (key, value) {
            if (value.type == 'text' ||
                value.type == 'file' ||
                value.type == 'hidden' ||
                value.type == 'password' ||
                value.type == 'textarea' ||
                //value.type == 'select' ||
                //value.type == 'select-one' ||
                // value.type == 'select-multiple'||
                value.type == 'date') {
                //if (value.value != '')
                //    obj[value.id] = value.value;

                $(this).val('');
            }
            else if (value.type == 'select' || value.type == 'select-one') {
                $(this).val('0');
            }
            else if (value.type == 'checkbox' || value.type == 'radio') {
                //if ($("#" + value.id).is(":checked")) {
                //    obj[value.name] = value.value;
                //}
                $(this).attr('checked', false);
            }
            else if (value.type == 'select-multiple') {
                // $(this)[0].sumo.unload();

                var obj = [];
                $('#' + this.id + ' option:selected').each(function () {
                    obj.push($(this).index());
                });

                for (var i = 0; i < obj.length; i++) {
                    //$(this).sumo.unSelectItem(obj[i]);
                    $('#' + this.id)[0].sumo.unSelectItem(obj[i])
                }
            }
        });

    }
}

function EnableDisable_DropdownValues(ctrl, value, flag) {
    $("#" + ctrl + " option[value=" + value + "]").prop('disabled', flag);
}

function Remove_DropdownValues(ctrl, value) {
    $("#" + ctrl + " option[value='" + value + "']").remove();
}

/* Validate not enter the future date */
function FutureDateValidation(date, ctrl) {
    if (date != '') {
        maxDate = new Date();
        var dt = maxDate.getDate() + "-" + GetMonthName(maxDate.getMonth() + 1) + "-" + maxDate.getFullYear();
        if (maxDate.getDate() < 10)
            dt = "0" + maxDate.getDate() + "-" + GetMonthName(maxDate.getMonth() + 1) + "-" + maxDate.getFullYear();
        if (Date.parse(JSONDate3(date)) <= Date.parse(JSONDate3(dt))) {
            // if (date <= dt) {
            if (date.length == 10) {
                date = date.getDate() + "-" + GetMonthName(date.getMonth() + 1) + "-" + date.getFullYear();
                $('#' + ctrl).val(date);
            }
            return true;
        }
        else {
            DialogWarningMessage('Future date should not be allowed.');
            $('#' + ctrl).val('');
            return false;
        }
    }
}


/*-------------------------------------------------
Code Written By B. Srinu  on 18-Jan-2016
Description : Bug List Dialog open
---------------------------------------------------*/
function BugListDialog() {
    //e.preventDefault();
    //$('#dialogbugInnerText').text(innertext);
    GetBugList();
    $("#dialog-bugtrackerList").removeClass('hide').dialog({
        resizable: false,
        width: '1050',
        height: 600,
        modal: true,
        title: "Bug List",
        //title:"<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
        title_html: true,
        buttons: [
            //{
            //    html: "<i class='ace-icon fa fa-save-o bigger-110'></i>&nbsp; Proceed",
            //    "class": "btn btn-danger btn-minier",
            //    click: function () {
            //        Reportbug();
            //       // $('#txtbugDesc').val('');
            //        $('#txtloginuserName').val('');
            //        $(this).dialog("close");
            //        //  if (successmethod != null) successmethod();
            //        return true;

            //    }
            //}
            //,
            {
                html: "<i class='ace-icon fa fa-times bigger-110'></i>&nbsp; Close",
                "class": "btn btn-minier",
                click: function () {
                    $(this).dialog("close");
                    return false;
                }
            }
        ]
    });
}

/*-------------------------------------------------
Code Written By B. Srinu  on 01-Oct-2015
Description : Insert Bug or observation Description 
---------------------------------------------------*/
function GetBugList() {
    try {
        // var desc = $('#txtbugDesc').val();
        var user = '';// $('#txtloginuserName').val();
        // var pagename = "";
        //if (desc != '' && user != '') {
        $.ajax({
            url: '/Common/GetUserBugs',
            type: 'POST',
            data: { Loginname: user },
            success: function (result) {
                CheckSessionVariable(result);
                BindGridResults($.parseJSON(result), 'tblbugList', 10);
                // alert(result);
                // DialogResultMessage("Your observation is recorded successfully...");
            },
            error: function () {

                DialogCommomErrorFunction('Error while Processing')
            }
        });
        //} else {
        //    DialogWarningMessage('Username and description fields are mandatory')
        //}
    } catch (e) {
        DialogCommomErrorFunction('Error while Processing')
    }
}

//-------------------------------Bug Description Insert  END

function ValidateLogo(id) {
    var res_field = $('#' + id).val();// document.myform.elements[id].value;
    var extension = res_field.substr(res_field.lastIndexOf('.') + 1).toLowerCase();
    //var allowedExtensions = ['doc', 'docx', 'txt', 'pdf', 'rtf'];
    // var fname = "the file name here.ext";
    var re = /(\jpg|\jpeg|\bmp|\gif|\png)$/i;
    if (!re.exec(extension)) {
        alert("File extension not supported!");
        $('#' + id).val('')
        return false;
    }
}

/* Converting 'Wed Jan 27 2016 00:00:00 GMT+0530' to dd-MMM-yyyy */
function ConvertNewDateToDate(date, ctrl) {
    if (date != '') {
        date = date.getDate() + "-" + GetMonthName(date.getMonth() + 1) + "-" + date.getFullYear();
        $('#' + ctrl).val(date);
    }
}

/* Add one year to given date */
function AddOneYearToDate(date, ctrl) {
    if (date != '') {
        var splits = date.split("-");
        var dateNew = new Date(splits[1] + "-" + splits[0] + "-" + splits[2]);
        //alert (splits[2])
        dateNew.setDate(dateNew.getDate() + 365);
        ConvertNewDateToDate(dateNew, ctrl);
    }
}
function BlockDotandComma() {
    if (event.keyCode == 46 || event.keyCode == 44 || event.ctrlKey == true) {
        event.returnValue = false;
    }
}

function GetRegionNamebyId(id) {
    var _name = '';
    $.each(MasterData.RegionData, function (i, item) {
        if (item.ID == id)
            _name = item.Name;
    })
    return _name;
}



function BindRequestActions_CS_CRM(data, requestID, flag) {
    if (data.length > 0) {
        data = $.parseJSON(data);
        $('#tblRequestActions tbody').html('');
        for (var i = 0; i < data.length; i++) {
            var row = $('<tr>');

            row.append($('<td align="left">').html((i + 1)));
            //if (data[i].RoleID == 2) {
            if (flag == 0)
                row.append($('<td class="left">').html('<a  onclick="myFunction(' + requestID + ',' + data[i].RoleID + ')" >' + data[i].Name + '</a>'));
            else
                row.append($('<td class="left">').html('<a >' + data[i].Name + '</a>'));
            ////row.append($('<td>').html('<a onclick="myPopup(\'/CRM/crmview/\',' + requestID + ')" >' + data[i].RoleDesc + '</a>'));
            //} else {
            //    row.append($('<td align="left">').html(data[i].Name));
            //}
            row.append($('<td align="left">').html(data[i].DeptName));
            // row.append($('<td>').html('Srinu B'));
            row.append($('<td align="left">').html(JSONDateTime(data[i].OpenDate)));
            row.append($('<td align="left">').html(JSONDateTime(data[i].ClosedDate)));
            //row.append($('<td>').html('<td><a href="CS/createrequest/' + data[i].REQUESTID + '" >Edit</a></td>'));
            $('#tblRequestActions tbody').append(row);
        }
    } else {
        $('#tblRequestActions tbody').html('');
        $('#tblRequestActions tbody').append('<tr><th colspan="10"  ><div align="center" style=" align:center">No Records Found</div></th></tr>');
    }
}

function myFunction(id, RoleID) {
    if (RoleID == 2)
        window.open("/CS/CSRequest?Id=" + id + "&IsView=1", "", "scrollbars=1,resizable=1,width=1000,height=580,left=0,top=0");
    else if (RoleID == 3)
        window.open("/CRM/CRMRequest?Id=" + id + "&IsView=1", "", "scrollbars=1,resizable=1,width=1000,height=580,left=0,top=0");
}

function CommonDialog_WithParameter(innerhtml, titletext, successmethod, closemethod, _width, _height, param) {
    //e.preventDefault();
    $('#divCommonalertmessage').html('');
    $('#dialogcommonInnerText').html(innerhtml);
    $("#dialog-common").removeClass('hide').dialog({
        resizable: false,
        width: _width,
        height: _height,
        modal: true,
        title: titletext,
        //title:"<div class='widget-header'><h4 class='smaller'><i class='ace-icon fa fa-exclamation-triangle red'></i> Empty the recycle bin?</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "&nbsp; OK",//"<i class='ace-icon fa fa-trash-o bigger-110'></i>&nbsp; Proceed",
                "class": "btn btn-danger btn-minier",
                click: function () {
                    $(this).dialog("close");
                    if (successmethod != null) successmethod(param);
                    return true;
                }
            }
            ,
            {
                html: "<i class='ace-icon fa fa-times bigger-110' id='btncommondialogclose'></i>&nbsp; Close",
                "class": "btn btn-minier",
                click: function () {
                    if (closemethod != null && closemethod != '')
                        closemethod();
                    $(this).dialog("close");
                    return false;
                }
            }
        ]
    });
}


function DateRangeValidation(dtinput, inBound, outBound, controlname) {
    //if (dtinput == "") {
    //    DialogWarningMessage('Please Enter/Provide Date to validate');
    //    return false;
    //}
    //else
    if (inBound == "" || outBound == "") {
        DialogWarningMessage('Please provide Dates to check date Range validation');
        return false;
    } else {
        var startrange = new Date(inBound);
        var endrange = new Date(outBound);
        var inputdt = new Date(dtinput);
        if (inputdt < startrange || inputdt > endrange) {
            DialogWarningMessage("Date should be between " + inBound + " and  " + outBound + "");
            $('#' + controlname).val();
            return false;
        } else {
            return true;
        }
    }
}

/* Claim Information sheet view */
function ClaimsInformationSheet_View(_ClaimID, _SlNo) {

    $.ajax({
        type: "GET",
        url: "/MedicalScrutiny/ClaimInformationSheet_Retrieve",
        contentType: 'application/json;charset=utf-8',
        //processData: false,
        data: { ClaimID: _ClaimID, SlNo: _SlNo },
        success: function (data) {
            CheckSessionVariable(data);
            data = $.parseJSON(data);

            if (data == null || data == "") {
                //alert('Data not found.');
            }
            else
                Bind_ClaimsInformationSheet(data);
        },
        error: function (e, x) {
            ShowResultMessage('ErrorMessage', e.responseText);
        }
    });

}

function Bind_ClaimsInformationSheet(data) {
    $('#tblCI_PreAuthorization tbody,#tblCI_ClaimDetails tbody,#tblCI_Diagnosis tbody').html('');

    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1;
    curr_month = GetMonthName(curr_month);
    var curr_year = d.getFullYear();

    $("#spnCI_Date").text(curr_date + ' ' + curr_month + ' ' + curr_year);

    if (data.Table.length > 0) {
        $("#spnCI_UHIDNo").text(data.Table[0].PatientUHID);
        $("#spnCI_ClaimID").text(data.Table[0].ClaimID);
        $("#spnCI_ClaimType").text(data.Table[0].ClaimType);

        $("#spnCI_IntimationID").text(data.Table[0].IntimationNo);
        $("#spnCI_IntimationDate").text(data.Table[0].IntimationDate);
        $("#spnCI_ServiceType").text(data.Table[0].ServiceType);

        $("#spnCI_CorporateName").text(data.Table[0].Corporate);
        $("#spnCI_MemberJoiningDate").text(data.Table[0].MemberJoinDate);
        $("#spnCI_PolicyPeriod").text(data.Table[0].PolicyPeriod);

        $("#spnCI_PatientName").text(data.Table[0].PatientName);
        $("#spnCI_MemberName").text(data.Table[0].MainMemberName);

        $("#spnCI_Age").text(data.Table[0].Age);
        $("#spnCI_Relationship").text(data.Table[0].RelationShip);

        $("#spnCI_PolicyNo").text(data.Table[0].PolicyNo);
        $("#spnCI_Status").text(data.Table[0].Status);
        $("#spnCI_Bonus").text(data.Table[0].CummulativeBonus);

        $("#spnCI_Insurer").text(data.Table[0].Insurer);
        $("#spnCI_SumInsured").text(data.Table[0].SumInsured);

        $("#spnCI_HospitalName").text(data.Table[0].HospitalName);
        $("#spnCI_ClaimedAmount").text(data.Table[0].ClaimedAmount);

        $("#spnCI_AdmissionDate").text(data.Table[0].AdmissionDate);
        $("#spnCI_DischargeDate").text(data.Table[0].DischargeDate);

        $("#spnCI_ProposerName").text(data.Table[0].ProposerName);
        $("#spnCI_BillNo").text(data.Table[0].BillNo);

        var RequestType = getNamepropwithId(data.Table[0].RequestTypeID, MasterData.mClaimRequestType["Result"]);
        $("#spnCI_ClaimRequestType").text(RequestType);

        //$("#spnCI_DoctorNotes").text(data.Table[0].);
        $("#spnCI_OrganizationNotes").text(data.Table[0].OrganisationNote);
        $("#spnCI_MemberNotes").text(data.Table[0].Notes);
    }

    /* Preauth Details */
    if (data.Table1.length > 0) {
        for (var i = 0; i < data.Table1.length; i++) {
            var tblBody = '<tr> <td class="numeric" style="text-align: left;">' + data.Table1[i].PreAuthID + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table1[i].UHID + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table1[i].AdmissionDate + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table1[i].Diagnosis + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table1[i].Hospital + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table1[i].Status + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table1[i].AuthorizedAmount + '</td>'
                + '</tr>';
            $('#tblCI_PreAuthorization tbody').append(tblBody);
        }
    }
    else
        $('#tblCI_PreAuthorization tbody').append('<tr> <td colspan=7 style="text-align: center;">No records found.</td></tr>');


    /* Claim Details */
    if (data.Table2.length > 0) {
        for (var i = 0; i < data.Table2.length; i++) {
            var tblBody = '<tr> <td class="numeric" style="text-align: left;">' + data.Table2[i].ClaimID + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].PatientUHID + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].AdmissionDate + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].Hospital + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].ClaimedAmount + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].Status + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].ClaimSettledAmount + '</td>'
                + '</tr>';
            $('#tblCI_ClaimDetails tbody').append(tblBody);
        }
    }
    else
        $('#tblCI_ClaimDetails tbody').append('<tr> <td colspan=7 style="text-align: center;">No records found.</td></tr>');

    /* Claim Diagnosis */
    if (data.Table2.length > 0) {
        for (var i = 0; i < data.Table2.length; i++) {
            var tblBody = '<tr> <td class="numeric" style="text-align: left;">' + data.Table2[i].ClaimID + '</td>'
                + '<td  class="numeric" style="text-align: left;">' + data.Table2[i].Diagnosis + '</td>'
                + '</tr>';
            $('#tblCI_Diagnosis tbody').append(tblBody);
        }
    }
    else
        $('#tblCI_Diagnosis tbody').append('<tr> <td colspan=7 style="text-align: center;">No records found.</td></tr>');

}
