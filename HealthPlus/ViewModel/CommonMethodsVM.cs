﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HealthPlus.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace HealthPlus.ViewModel
{
    public class CommonMethodsVM
    {
        DBHelper.DBHelper vDBHelper = null;
        public CommonMethodsVM()
        {
            vDBHelper = new DBHelper.DBHelper(ConfigurationManager.AppSettings["sqlCon"].ToString());
        }

        public void ReadMasterData()
        {
            System.Web.HttpContext.Current.Application.Lock();
            LoadMasterTables();
            System.Web.HttpContext.Current.Application.UnLock();
        }

        public void LoadMasterTables()
        {
            DataTable vDt = new DataTable();
            vDt = GetMasterData("Mst_Property");

            DataTable dt;
            for (int i = 0; i < vDt.Rows.Count; i++)
            {
                dt = new DataTable();
                dt = GetMasterDataWithSingleParameter(Convert.ToInt32(vDt.Rows[i]["ID"].ToString()), "Mst_PropertyValues", "@PropertyId");
                System.Web.HttpContext.Current.Application[vDt.Rows[i]["Name"].ToString()] = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            }


        }


        public DataTable GetMasterData(string TableName)
        {
            try
            {
                string sqlQuery = string.Format(DBQueriesVM.GetMasterData, TableName);
                DataTable dt = new DataTable();
                vDBHelper.mCreateCommand(CommandType.Text, sqlQuery);
                dt = vDBHelper.ExecuteDT();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                
        public DataTable GetMasterDataWithSingleParameter(int PropertyId, string TableName,string Parameter)
        {
            try
            {
                vDBHelper = new DBHelper.DBHelper(ConfigurationManager.AppSettings["sqlCon"].ToString());

                string sqlQuery = string.Format(DBQueriesVM.GetProperValues, TableName);
                DataTable dt = new DataTable();
                vDBHelper.mCreateCommand(CommandType.Text, sqlQuery);
                vDBHelper.mAddParameter(Parameter, SqlDbType.Int, ParameterDirection.Input, PropertyId);
                dt = vDBHelper.ExecuteDT();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}