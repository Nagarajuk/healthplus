﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthPlus.Models
{
    public class MasterProperties_Model
    {
    }
    public class MasterData_Int
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class MasterData_Long
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }

    public class MasterData_District
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StateID { get; set; }
    }

    public class MasterData_TinyInt
    {
        public Int16 ID { get; set; }
        public string Name { get; set; }
    }
}