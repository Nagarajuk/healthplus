﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BAL
{
    class MasterData_BL
    {
        public List<MasterData_Int> GetAllProperties()
        {
            try
            {

                IList<Mst_Property> Results = (from res in context.Mst_Property where res.Deleted == false select res).ToList();

                return Results;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Reading Mst_PropertyValues Table By Property ID
        public List<MappingTable> GetMasterByPropertyID(int PropertyId)
        {
            using (var context = new McarePlusEntities())
            {
                // var userSuppliedId = new SqlParameter("@PropertyId", PropertyId);
                parametersValue = new object[1];
                parametersValue[0] = new SqlParameter("@PropertyId", PropertyId);
                string sqlQuery = DBQueries.MasterByPropertyID_Query;
                //string sqlQuery = @"SELECT ID,Name FROM Mst_PropertyValues where PropertyID=@PropertyId and Deleted=0";
                List<MappingTable> Results = context.Database.SqlQuery<MappingTable>(sqlQuery, parametersValue).ToList();
                return Results;
            }
        }

    }
}
